from PyQt4.QtGui import QApplication, QMainWindow, QFrame, QGridLayout, QWidget, QScrollBar, QLabel, QTabWidget, QPushButton, QRadioButton, QHBoxLayout, QSpinBox, QFileDialog, QComboBox, QGroupBox, QVBoxLayout, QDial, QDialog, QSlider, QMenu, QLineEdit
from PyQt4.QtCore import Qt, QFile, QLatin1String, QSize
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
import vtk, sys, dicom, numpy as np, glob, xml.etree.ElementTree as ET, os, datetime, colorsys
from ui.widgets.transferfunction import TransferFunction, TransferFunctionWidget
from PySide.QtGui import QDialog as pysideQWidget
from PySide.QtGui import QGridLayout as pysideQGridLayout
import vrpn
import math
from astropy.units import count
from _sqlite3 import Cursor

isprojector = False
initfdir = ''
initddir = ''
tfuncdir = 'Presets\\TransferFunctions\\'
settings_dir = 'settings'
screenshot_dir = 'screenshots'

calibArray = np.array([1.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.541127, -0.840941, 0.000000, 0.000000, 0.840941, 0.541127, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000])
calibMat_np = np.reshape(calibArray, (4,4))
# calibMatrix = vtk.vtkMatrix4x4().DeepCopy(calibMat_np.reshape(16,1))
# calibTransform = vtk.vtkTransform()
# calibTransform.Concatenate(calibMatrix)


class TDViz(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        
        self.frame = QFrame()
        self.vtkWidget = QVTKRenderWindowInteractor(self.frame)
        
        self.cropControlItems = CropControlItems(self)
        self.commonControlItems = CommonControlItems(self)
        self.positionControlItems = PositionControlItems(self)
        self.transferFunctionControlItems = TransferFunctionControlItems(self)
        self.planeWidgetControlItems = PlaneWidgetControlItems()
        self.playControlItems = PlayControlItems(self)
        self.smoothingControlItems = SmoothingControlItems(self)
        self.lightingControlItems = LightingControlItems()
        self.viewControlItems = ViewControlItems(self)
        self.labelControlItems = LabelControlItems()
        self.splitControlItems = SplitControlItems(self)  
        
        tabWidget = QTabWidget()
        tabWidget.addTab(self.commonControlItems, "General Controls")
        tabWidget.addTab(self.viewControlItems, "View Controls")
        tabWidget.addTab(self.cropControlItems, "Cropping XYZ")
        tabWidget.addTab(self.planeWidgetControlItems, "Cropping Planes")
        tabWidget.addTab(self.transferFunctionControlItems, "Opacity && Color")
        tabWidget.addTab(self.positionControlItems, "Rotation && Position")
        tabWidget.addTab(self.playControlItems, "Play")
        tabWidget.addTab(self.smoothingControlItems, "Smoothing")
        tabWidget.addTab(self.lightingControlItems, "Lighting")
        tabWidget.addTab(self.labelControlItems, "Labeling")
        tabWidget.addTab(self.splitControlItems, "Splitting Controls")
        
        buttonGroup = QGroupBox()
        self.button_quit = QPushButton("Close")
        self.button_savesettings = QPushButton("Save Settings")
        self.label_loadsettings = QLabel("Load Settings: ")
        self.label_loadsettings.setAlignment(Qt.AlignTrailing)
        self.combobox_loadsettings = QComboBox()
        
        buttonLayout = QGridLayout()
        for index, button in enumerate((self.label_loadsettings, self.combobox_loadsettings, self.button_savesettings, self.button_quit)):
            buttonLayout.addWidget(button, index/2, index % 2 )
        buttonLayout.setSpacing(0)
        buttonLayout.setColumnStretch(0,4)
        buttonLayout.setColumnStretch(1,6)        
        
        buttonGroup.setLayout(buttonLayout)
        
        topButtonGroup = QGroupBox()
        self.button_thing1 = QRadioButton("thing1")
        self.button_thing2 = QRadioButton("thing2")
        topButtonLayout = QGridLayout()
        topButtonLayout.addWidget(self.button_thing1, 0,0)
        topButtonLayout.addWidget(self.button_thing2, 0,1)
        topButtonGroup.setLayout(topButtonLayout)
        
        layout = QGridLayout()
        layout.addWidget(topButtonGroup)
        layout.addWidget(self.vtkWidget, 1, 0, 1, 2)
        layout.setRowStretch(1, 1)
        layout.addWidget(tabWidget, 2, 0)
        layout.addWidget(buttonGroup, 2, 1)
        layout.setSpacing(0)
        layout.setMargin(0)
        layout.setColumnStretch(0,10)
        layout.setColumnStretch(1,3)
        
        self.frame.setLayout(layout)
        self.setCentralWidget(self.frame)
        
        self.button_thing1.clicked.connect(self.thing1)
        self.button_thing2.clicked.connect(self.thing2)
        
        self.button_quit.clicked.connect(self.close)
        self.button_loadEcho.clicked.connect(self.loadEcho)
        self.button_box.clicked.connect(self.setBoxWidget)
        self.transferFunctionControlItems.combobox_transfunction.activated.connect(self.updateTFunc)
        self.button_resetcrop.clicked.connect(self.resetCrop)
        self.button_stereo.clicked.connect(self.setStereo)
        self.button_measurement.clicked.connect(self.setMeasurement)
        self.button_anglemeasurement.clicked.connect(self.setAngleMeasurement)
        self.button_loadDir.clicked.connect(self.loadDir)
        self.button_savesettings.clicked.connect(self.saveSettings)
        self.button_iterate.clicked.connect(self.playCardiacCycle)
        self.button_rotate.clicked.connect(self.rotateCamera)
        self.transferFunctionControlItems.button_edittransfunction.clicked.connect(self.editTransferFunction)
        self.transferFunctionControlItems.button_editopacity.clicked.connect(self.editOpacity)
        self.transferFunctionControlItems.button_editcolor.clicked.connect(self.editColor)
        self.transferFunctionControlItems.button_editgradient.clicked.connect(self.editGradientOpacity)
        self.transferFunctionControlItems.button_savetfunction.clicked.connect(self.saveTransferFunction)
        
        self.button_test0.clicked.connect(self.test0)
        self.button_test1.clicked.connect(self.test1)
        self.button_cursor.clicked.connect(self.setSplitPlaneWidget)
        self.button_split.clicked.connect(self.setSplitVolume)
        self.button_report.clicked.connect(self.report)

        self.button_savescreen.clicked.connect(self.saveScreen)
        self.combobox_loadsettings.activated.connect(self.loadSettings)
        
        for scale in (self.scale_xmin, self.scale_xmax, self.scale_ymin, self.scale_ymax, self.scale_zmin, self.scale_zmax):
            scale.valueChanged.connect(self.cropVolume)
            
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.valueChanged.connect(self.smoothVolume)
            
        self.button_nosmooth.clicked.connect(self.setNoSmooth)
        self.button_lowsmooth.clicked.connect(self.setLowSmooth)
        self.button_midsmooth.clicked.connect(self.setMidSmooth)
        self.button_highsmooth.clicked.connect(self.setHighSmooth)            

        self.scale_azimuth.valueChanged.connect(self.setAzimuth)
        self.scale_elevation.valueChanged.connect(self.setElevation)
        self.scale_roll.valueChanged.connect(self.setRoll)
        self.scale_stereodepth.valueChanged.connect(self.setStereoDepth)
        
        self.button_zoomin.clicked.connect(self.zoomIn)
        self.button_zoomout.clicked.connect(self.zoomOut)
        self.button_resetcamera.clicked.connect(self.resetCamera)
        
        self.slider_imageNumber.valueChanged.connect(self.slider_imageNumber_valuechanged)
        
        for i in range(6):
            self.planeWidgetControlItems.button_pwidgets[i].toggled.connect(self.setPlaneWidgets)
            self.planeWidgetControlItems.button_pwidgetreset[i].clicked.connect(self.resetPlaneWidget)
            
        self.planeWidgetControlItems.button_pwdigetresetall.clicked.connect(self.resetAllPlaneWidgets)
        self.button_cameratext.toggled.connect(self.displayCameraOrientation)
        
        self.lightingControlItems.button_shade.toggled.connect(self.setShade)
        self.lightingControlItems.button_interpolation.toggled.connect(self.setInterpolation)
        self.lightingControlItems.button_gradientopacity.toggled.connect(self.setDisableGradientOpacity)
        self.lightingControlItems.slider_ambient.valueChanged.connect(self.adjustLights)
        self.lightingControlItems.slider_diffuse.valueChanged.connect(self.adjustLights)
        self.lightingControlItems.slider_specular.valueChanged.connect(self.adjustLights)
        
        self.lightingControlItems.slider_keylightintensity.valueChanged.connect(self.setKeyLightIntensity)
        
        self.labelControlItems.button_label.toggled.connect(self.displayLabel)
        self.labelControlItems.text_label.textEdited.connect(self.changeLabelText)
        self.labelControlItems.scale_labelsize.valueChanged.connect(self.changeLabelSize)
        self.labelControlItems.combobox_labels.currentIndexChanged[int].connect(self.changeLabelIndex)
    
        for button in self.button_view:
            button.clicked.connect(self.changeView)
    
class vtkTimerHeadTrack():
    tracker=vrpn.receiver.Tracker("Tracker0@localhost")
    button=vrpn.receiver.Button("Tracker0@localhost")
    
    def __init__(self, camera, headtrackText, stylusText, cursor, volume0, volume1, master):
        self.headtrackText = headtrackText
        self.stylusText = stylusText
        self.tracker.register_change_handler("tracker", self.callback, "position")
        self.button.register_change_handler("button", self.callback_button)

        self.cursor = cursor
        self.camera = camera
        self.master = master
        self.volume0 = volume0
        self.volume1 = volume1
        self.spw = master.splitPlaneWidget0
        
        self.deviceMatrixCalibrated_np = np.zeros((4,4))
        self.deviceMatrix_np = np.zeros((4,4))
        self.deviceMatrix = vtk.vtkMatrix4x4()
        self.deviceMatrixCalibrated = vtk.vtkMatrix4x4()
        self.cameraModelViewTransform = vtk.vtkTransform()
        
        self.deviceMatrixCalibrated_vtk = vtk.vtkMatrix4x4()

        self.deviceTransformCalibrated = vtk.vtkTransform()
        self.deviceTransformCalibrated.PostMultiply()
        
        cameraModelViewMatrix = self.camera.GetModelViewTransformMatrix()
        self.cameraModelViewTransform.Concatenate(self.camera.GetModelViewTransformMatrix())      
        
        self.buttonDown0 = False
        self.buttonDown1 = False        
        self.buttonDown2 = False
        self.buttonHeld0 = False
        self.buttonHeld1 = False
        self.buttonHeld2 = False
            
        self.initialZPosition = None
        self.initialScaleTransform = None
                
    def execute(self, obj, event):
        iren = obj
        self.button.mainloop()
        self.tracker.mainloop()           
        iren.GetRenderWindow().Render()   
        
    def callback_button(self, userdata, data):
        if data['button'] == 0:
            if data['state'] == 1:
                self.cursor.GetProperty().SetColor(1.0,0.0,0.0)
                self.buttonDown0 = True
            elif data['state'] == 0:
                self.cursor.GetProperty().SetColor(1.0,1.0,1.0)
                self.buttonDown0 = False                  
        if data['button'] == 1:
            if data['state'] == 1:
                self.cursor.GetProperty().SetColor(0.0,0.0,1.0)
                self.buttonDown1 = True
            elif data['state'] == 0:
                self.cursor.GetProperty().SetColor(1.0,1.0,1.0)
                self.buttonDown1 = False                   
        if data['button'] == 2:
            if data['state'] == 1:
                self.cursor.GetProperty().SetColor(0.0,1.0,0.0)
                self.buttonDown2 = True
            elif data['state'] == 0:
                self.cursor.GetProperty().SetColor(1.0,1.0,1.0)
                self.buttonDown2 = False                             
        
    def callback(self, userdata, data):
        
        dx, dy, dz = data['position']
        qx, qy, qz, qw = data['quaternion']        
        sensorid = data['sensor']      
        qwxyz = np.array([qw,qx,qy,qz])              
        vtk.vtkMath.QuaternionToMatrix3x3(qwxyz, self.deviceMatrix_np[0:3,0:3])
        self.deviceMatrix_np[0,3] = 1000*dx
        self.deviceMatrix_np[1,3] = 1000*dy
        self.deviceMatrix_np[2,3] = 1000*dz
        self.deviceMatrix_np[3,3] = 1.0
        
        self.deviceMatrixCalibrated_np = calibMat_np.dot(self.deviceMatrix_np)

        #glasses
        if sensorid == 0:
            if not math.isnan(dx) and not math.isnan(qx):
                self.camera.SetEyeTransformMatrix(self.deviceMatrixCalibrated_np.reshape(16,1))  #deviceMatrixCalibrated_np -> calibMat_np  
                self.headtrackText.SetInput("test = (%-#6.3g, %-#6.3g, %-#6.3g)\n quaternion = (%-#6.3g, %-#6.3g, %-#6.3g, %-#6.3g)" % (dx, dy, dz, qw, qx, qy, qz))
                self.camera.Modified()  
                         
        #stylus
        elif sensorid == 1:       
            self.deviceMatrixCalibrated_vtk.DeepCopy(self.deviceMatrixCalibrated_np.reshape(16,1))   
            
            cursorTransform = vtk.vtkTransform()
            cursorTransform.PostMultiply()
            cameraTransform = vtk.vtkTransform()
            cameraTransform = vtk.vtkTransform()
            
            cursorTransform.Concatenate(self.deviceMatrixCalibrated_vtk)                      
            cameraTransform.Concatenate(self.camera.GetModelViewTransformMatrix())                
            cursorTransform.Concatenate(cameraTransform.GetInverse())     
                             
            self.cursor.SetUserTransform(cursorTransform)
            self.cursor.Modified()
            
            if self.buttonDown0:
                if self.buttonHeld0 == False:
                    cursorPosition = self.getStylusPosition()
                    self.volume = self.getClosestVolume(cursorPosition)
                    self.volumeSaveOldTransform()
                    self.cursorSaveOldTransform()
                    self.buttonHeld0 = True  
                else:
                    volumeTransform = self.volumeCalculateTransform()
                    self.volume.SetUserTransform(volumeTransform)   
            else:
                if self.buttonHeld0 == True:
                    self.buttonHeld0 = False

                
            if self.master.volume1Enabled == False:#button only works if volume not split
                if self.buttonDown1:
                    if self.buttonHeld1 == False:
                        self.buttonHeld1 = True
                        self.spwSaveOldNormalAndOrigin()
                        self.cursorSaveOldTransform()
                    else:
                        spwNormalNew, spwOriginNew = self.spwCalculateNormalAndOrigin(cursorTransform)
                        self.spw.SetOrigin(spwOriginNew)
                        self.spw.SetNormal(spwNormalNew)
                        self.spw.Modified()
                else:
                    if self.buttonHeld1 == True:
                        self.buttonHeld1 = False
                        self.master.spwPlane0Callback()
                                   
            if self.buttonDown2:
                if self.master.distanceWidget.GetEnabled():
                    self.distanceWidgetInteraction(cursorTransform)
                elif self.master.angleWidget.GetEnabled():
                    self.angleWidgetInteraction(cursorTransform)    
                else:
                    self.planeWidgetInteraction(cursorTransform)   

    def getStylusPosition(self):
        stylusOrigin = np.array((0,0,-100))
        stylusPosition = np.empty((3))
        self.cursor.GetUserTransform().TransformPoint(stylusOrigin, stylusPosition)
        return stylusPosition
    
    def getClosestVolume(self, point):
        volumePosition0 = self.volume0.GetCenter()
        volumePosition1 = self.volume1.GetCenter()
        distanceTo0 = vtk.vtkMath.Distance2BetweenPoints(volumePosition0, point)
        distanceTo1 = vtk.vtkMath.Distance2BetweenPoints(volumePosition1, point) 
        if self.master.volume1Enabled and distanceTo1 < distanceTo0:
            return self.volume1
        else:
            return self.volume0
        
    def cursorSaveOldTransform(self):
        self.cursorTransformOld = vtk.vtkTransform()
        self.cursorTransformOld.DeepCopy(self.cursor.GetUserTransform())
        
    def volumeSaveOldTransform(self):
        self.volumeTransformOld = vtk.vtkTransform() 
        self.volumeTransformOld.DeepCopy(self.volume.GetUserTransform())
        
    def spwSaveOldNormalAndOrigin(self):
        self.spwOriginOld = np.empty((3))
        self.spwNormalOld = np.empty((3))
        for i in range(3):
            self.spwOriginOld[i] = self.spw.GetOrigin()[i] 
            self.spwNormalOld[i] = self.spw.GetNormal()[i]
            
    def volumeCalculateTransform(self):
        volumeTransform = vtk.vtkTransform() 
        volumeTransform.DeepCopy(self.volumeTransformOld)
        volumeTransform.PostMultiply()
        volumeTransform.Concatenate(self.cursorTransformOld.GetInverse())
        volumeTransform.Concatenate(self.cursor.GetUserTransform())
        return volumeTransform
    
    def spwCalculateNormalAndOrigin(self, cursorTransform):
        self.planeTransform = vtk.vtkTransform()
        self.planeTransform.PostMultiply()
        self.planeTransform.Concatenate(self.cursorTransformOld.GetInverse())
        self.planeTransform.Concatenate(cursorTransform)
        spwOriginNew = np.empty((3))
        spwNormalNew = np.empty((3))                    
        self.planeTransform.TransformPoint(self.spwOriginOld, spwOriginNew)
        self.planeTransform.TransformNormal(self.spwNormalOld, spwNormalNew)
        return (spwNormalNew, spwOriginNew)

    def distanceWidgetInteraction(self, transform):
        pt, pt1, pt2 = np.empty((3)), np.empty((3)), np.empty((3))
        transform.TransformPoint(np.array([0,0,-100]),pt)
        self.master.distanceWidget.GetDistanceRepresentation().GetPoint1WorldPosition(pt1)
        self.master.distanceWidget.GetDistanceRepresentation().GetPoint2WorldPosition(pt2)
        if np.linalg.norm(pt-pt1) < np.linalg.norm(pt-pt2):                
            self.master.distanceWidget.GetDistanceRepresentation().SetPoint1WorldPosition(pt)
            self.dwPoint0Callback()
        else:
            self.master.distanceWidget.GetDistanceRepresentation().SetPoint2WorldPosition(pt)
            self.dwPoint1Callback()               
        self.master.distanceText.SetInput("distance = %-#6.3g mm" % self.master.distanceWidget.GetDistanceRepresentation().GetDistance()) 
        
    def angleWidgetInteraction(self, transform):
        pt, pt1, pt2, pt3 = [np.empty((3)) for i in range(4)]

        transform.TransformPoint(np.array([0,0,-100]),pt)
        
        self.master.angleWidget.GetAngleRepresentation().GetPoint1WorldPosition(pt1)
        self.master.angleWidget.GetAngleRepresentation().GetCenterWorldPosition(pt2)
        self.master.angleWidget.GetAngleRepresentation().GetPoint2WorldPosition(pt3)
        
        if np.linalg.norm(pt-pt1) <= np.linalg.norm(pt-pt2) and np.linalg.norm(pt-pt1) <= np.linalg.norm(pt-pt3):
            self.master.angleWidget.GetAngleRepresentation().SetPoint1WorldPosition(pt)
        elif np.linalg.norm(pt-pt2) <= np.linalg.norm(pt-pt1) and np.linalg.norm(pt-pt2) <= np.linalg.norm(pt-pt3):
            self.master.angleWidget.GetAngleRepresentation().SetCenterWorldPosition(pt)
        elif np.linalg.norm(pt-pt3) <= np.linalg.norm(pt-pt1) and np.linalg.norm(pt-pt3) <= np.linalg.norm(pt-pt2):
            self.master.angleWidget.GetAngleRepresentation().SetPoint2WorldPosition(pt)
            
        self.master.angleText.SetInput("angle = %-#6.3g degrees" % vtk.vtkMath.DegreesFromRadians(self.master.angleWidget.GetAngleRepresentation().GetAngle()))            
                                    
        
    def planeWidgetInteraction(self, transform):
        pt, normal_ = np.empty((3)), np.empty((3))
        transform.TransformPoint(np.array([0,0,-100]),pt)
        transform.TransformNormal(np.array([0,0,-1.0]),normal_)
        
        for i in range(6):
            if self.master.planeWidget[i].GetEnabled():
                self.master.planeWidget[i].SetOrigin(pt)
                self.master.planeWidget[i].SetNormal(normal_)
                self.master.planeWidget[i].UpdatePlacement() 
                
                break
            
        self.master.pwCallback(None,None)
              
class vtkTimerSpaceNav():
    analog=vrpn.receiver.Analog("device0@localhost")
    
    def __init__(self, ren):
        self.timer_count = 0
        self.ren = ren
        self.camera = self.ren.GetActiveCamera()
        self.transform = vtk.vtkTransform()
        self.analog.register_change_handler("analog", self.callback)
        
    def execute(self, obj, event):
        iren = obj
        self.analog.mainloop()
            
        iren.GetRenderWindow().Render()    
    
    def callback(self,userdata, data):
        ch = data['channel']
        self.camera.Azimuth(ch[5])
        self.camera.Elevation(ch[3])
        self.camera.Roll(ch[4])
        self.camera.OrthogonalizeViewUp()
        self.camera.ApplyTransform(self.transform)
        self.ren.ResetCameraClippingRange()

class CropControlItems(QWidget):
    def __init__(self, master):
        super(CropControlItems, self).__init__()
        
        nscales = 6
        
        master.scale_xmin, master.scale_xmax, master.scale_ymin, master.scale_ymax, master.scale_zmin, master.scale_zmax = [QScrollBar(Qt.Horizontal) for i in range(nscales)]
        label_xmin, label_xmax, label_ymin, label_ymax, label_zmin, label_zmax = [QLabel() for i in range(nscales)]
        
        master.button_resetcrop = QPushButton("Crop Reset")
        master.button_box = QPushButton("BoxWidget On/Off",)
        layout = QGridLayout()            
        for index, label, labeltext, labelcolor in zip(range(nscales), (label_xmin, label_xmax, label_ymin, label_ymax, label_zmin, label_zmax), ("X min", "X max", "Y min", "Y max", "Z min", "Z max"),\
                                                       ("red", "red", "green", "green", "blue", "blue")):
            label.setText(labeltext)
            label.setStyleSheet("QLabel {color:" + labelcolor +" ; }");
            layout.addWidget(label, 0, index)
            
        layout.addWidget(master.button_box, 0, index + 1)
                    
        for index, scale in enumerate((master.scale_xmin, master.scale_xmax, master.scale_ymin, master.scale_ymax, master.scale_zmin, master.scale_zmax, master.button_resetcrop)):            
            scale.setEnabled(False)
            layout.addWidget(scale, 1, index)

        master.button_box.setEnabled(False)
            
        layout.setMargin(5)
        layout.setHorizontalSpacing(20)       
        layout.setVerticalSpacing(0)                
        self.setLayout(layout)
        
class SplitControlItems(QWidget):
    def __init__(self, master):
        super(SplitControlItems, self).__init__()
        master.button_cursor = QPushButton("Plane On/Off")
        master.button_split = QPushButton("Split")
        master.button_report = QPushButton("Report")
        master.button_test0 = QPushButton("test0")
        master.button_test1 = QPushButton("test1")
        layout = QGridLayout()
        layout.addWidget(master.button_cursor, 0, 0)
        layout.addWidget(master.button_split, 1, 0)
        layout.addWidget(master.button_report, 0, 1)
        layout.addWidget(master.button_test0, 1, 1)
        layout.addWidget(master.button_test1, 2, 1)
        master.button_cursor.setEnabled(False)
        master.button_split.setEnabled(False)
        master.button_report.setEnabled(True)
        master.button_test0.setEnabled(True)
        master.button_test1.setEnabled(True)
        layout.setMargin(5)
        layout.setHorizontalSpacing(20)       
        layout.setVerticalSpacing(0)
        self.setLayout(layout)
        
        
class PlaneWidgetControlItems(QWidget):
    def __init__(self):
        super(PlaneWidgetControlItems, self).__init__()
        
        nwidgets = 6
        
        self.button_pwidgets = [QPushButton() for i in range(nwidgets)]
        self.button_pwidgetreset = [QPushButton() for i in range(nwidgets)]
        self.button_pwdigetresetall = QPushButton("Reset All")
        self.button_pwdigetresetall.setEnabled(False)
        layout = QGridLayout()  
        for i in range(nwidgets):
            self.button_pwidgets[i].setText("Plane Widget - " + str(i+1))
            self.button_pwidgets[i].setCheckable(True)
            self.button_pwidgets[i].setEnabled(False)
            layout.addWidget(self.button_pwidgets[i],0,i)
            
            self.button_pwidgetreset[i].setText("Reset - " + str(i+1))
            self.button_pwidgetreset[i].setObjectName('resetplane%d' % i)
            self.button_pwidgetreset[i].setEnabled(False)
            layout.addWidget(self.button_pwidgetreset[i],1,i)
        
        layout.addWidget(self.button_pwdigetresetall,1,i+1)
            
        layout.setMargin(5)
        layout.setVerticalSpacing(0)
                    
        self.setLayout(layout)          
        
class PlayControlItems(QWidget):
    def __init__(self, master):
        super(PlayControlItems, self).__init__()
        master.slider_imageNumber = QSlider(Qt.Horizontal)
        master.slider_imageNumber.setPageStep(1)
        master.label_imageNumber = QLabel('')
        master.label_imageNumber.setFixedWidth(80)
        master.button_iterate = QPushButton("Play/Stop")
        layout = QGridLayout() 
        layout.addWidget(master.label_imageNumber,0,0)
        layout.addWidget(master.slider_imageNumber,1,0)
        layout.addWidget(master.button_iterate,1,1)
        layout.setMargin(5)
                            
        for col, val in enumerate((4,1)):
            layout.setColumnStretch(col,val)  
            
        master.slider_imageNumber.setEnabled(False)
        master.button_iterate.setEnabled(False)      
    
        self.setLayout(layout)      

class SmoothingControlItems(QWidget):
    def __init__(self, master):
        super(SmoothingControlItems, self).__init__()    
        
        master.button_nosmooth = QPushButton('No Smoothing')
        master.button_lowsmooth = QPushButton('Low Smoothing')
        master.button_midsmooth = QPushButton('Medium Smoothing')
        master.button_highsmooth = QPushButton('High Smoothing')
        
        master.slider_xsmooth = QSlider(Qt.Horizontal)    
        master.slider_ysmooth = QSlider(Qt.Horizontal)
        master.slider_zsmooth = QSlider(Qt.Horizontal)
        
        master.label_xsmooth = QLabel('')
        master.label_ysmooth = QLabel('')
        master.label_zsmooth = QLabel('')

        layout = QGridLayout() 
        
        for index, button in enumerate((master.button_nosmooth, master.button_lowsmooth, master.button_midsmooth, master.button_highsmooth)):
            layout.addWidget(button, 1, index)
        
        layout.addWidget(master.label_xsmooth,0,index+1)
        layout.addWidget(master.label_ysmooth,0,index+2)
        layout.addWidget(master.label_zsmooth,0,index+3)
        
        layout.addWidget(master.slider_xsmooth,1,index+1)
        layout.addWidget(master.slider_ysmooth,1,index+2)
        layout.addWidget(master.slider_zsmooth,1,index+3)
        
        layout.setMargin(5)
        
        self.setLayout(layout) 
        
class LightingControlItems(QWidget):
    def __init__(self):
        super(LightingControlItems, self).__init__()
        
        self.button_shade = QPushButton('Shade On/Off')
        self.button_interpolation = QPushButton('Interpolation: Linear/NN ')
        self.button_gradientopacity = QPushButton('Gradient Opacity On/Off')

        for comp in (self.button_shade, self.button_interpolation, self.button_gradientopacity):
            comp.setCheckable(True)
        
        self.slider_ambient = QSlider(Qt.Horizontal)    
        self.slider_diffuse = QSlider(Qt.Horizontal)
        self.slider_specular = QSlider(Qt.Horizontal)                
        self.slider_keylightintensity = QSlider(Qt.Horizontal)
        self.slider_ambient.setValue(100.0)
        self.slider_diffuse.setValue(100.0)        
        self.slider_keylightintensity.setValue(20)
              
        for comp in (self.button_shade, self.button_interpolation, self.button_gradientopacity, self.slider_ambient, self.slider_diffuse, self.slider_specular, self.slider_keylightintensity):
            comp.setEnabled(False)
        
        self.label_ambient = QLabel('Ambient: 1.0')
        self.label_diffuse = QLabel('Diffuse: 1.0')
        self.label_specular = QLabel('Specular: 0.0')

        self.label_keylightintensity = QLabel('Key Light Intensity: 4.0')

        layout = QGridLayout() 
        
        for ind, comp in enumerate((self.label_ambient, self.label_diffuse, self.label_specular, self.label_keylightintensity)):
            layout.addWidget(comp,0,ind)
        
        for ind, comp in enumerate((self.slider_ambient, self.slider_diffuse, self.slider_specular, self.slider_keylightintensity)):
            layout.addWidget(comp,1,ind)
        
        layout.addWidget(self.button_interpolation,0,ind+1)
        layout.addWidget(self.button_gradientopacity,1,ind+1)
        layout.addWidget(self.button_shade,1,ind+2)
        
        layout.setMargin(5)
        layout.setVerticalSpacing(0)
                
        self.setLayout(layout) 
        
class ViewControlItems(QWidget):
    def __init__(self, master):
        super(ViewControlItems, self).__init__()
        
        nbuttons = 10
        master.button_view = [QPushButton() for i in range(nbuttons)]
        
        self.label_view = QLabel("View")
        self.label_roll = QLabel("Roll")
        
        buttontext = ["XY", "YZ", "ZX", "-XY", "-YZ", "-ZX", "0", "90", "180", "270"]
 
        layout = QGridLayout()
        
        for index in range(nbuttons):
            master.button_view[index].setText(buttontext[index])
            master.button_view[index].setObjectName('button_view%d' % index)
            layout.addWidget(master.button_view[index], 1, index)

        layout.addWidget(self.label_view,0,0)
        layout.addWidget(self.label_roll,0,6)
        
        layout.setMargin(5)
        layout.setHorizontalSpacing(5)       
        layout.setVerticalSpacing(0)                  
        self.setLayout(layout)
            
class LabelControlItems(QWidget):
    def __init__(self):
        super(LabelControlItems, self).__init__()
        
        nlabels = 5
        
        self.combobox_labels = QComboBox()
        self.label_label = QLabel("Label: ")
        self.label_text = QLabel("Text: ")
        self.text_label = QLineEdit("Label1")
        self.button_label = QPushButton("On/Off")
        
        self.scale_labelsize = QSlider(Qt.Horizontal)
        self.label_labelsize = QLabel("Label Size")
        self.scale_labelsize.setMinimum(1)
        self.scale_labelsize.setValue(20)
        
        self.button_label.setCheckable(True)
        
        for i in range(nlabels):
            self.combobox_labels.addItem("Label"+str(i+1))
            
        layout = QGridLayout()
        
        layout.addWidget(self.label_label,0,0)
        layout.addWidget(self.combobox_labels,1,0)
        layout.addWidget(self.label_text,0,1)
        layout.addWidget(self.text_label,1,1)
        layout.addWidget(self.button_label,1,2)

        layout.addWidget(self.label_labelsize,0,3)
        layout.addWidget(self.scale_labelsize,1,3)        
             
        for col, stretch in enumerate((5,5,5,5)):
            layout.setColumnStretch(col, stretch)            
        
        layout.setMargin(5)
        layout.setHorizontalSpacing(5)       
        layout.setVerticalSpacing(0)                  
        self.setLayout(layout)
                   
class CommonControlItems(QWidget):
    def __init__(self, master):
        self.master = master
        super(CommonControlItems, self).__init__()
        
        nbuttons = 8
        
        master.button_loadEcho, master.button_loadDir, master.button_rotate, \
            master.button_stereo, master.button_measurement, master.button_anglemeasurement,  master.button_cameratext, master.button_savescreen = [QPushButton() for i in range(nbuttons)]                    
        buttontext = ["Load Echo", "Load MR/CT",  "Rotate/Stop", "Stereo On/Off", "Distance Meas. On/Off", "Angle Meas. On/Off", "Display Orientation", "Save Screen"]
               
        master.button_cameratext.setCheckable(True)
        
        layout = QHBoxLayout() 
        layout.setSpacing(0)
        for index, button in enumerate((master.button_loadEcho, master.button_loadDir,  master.button_rotate, \
            master.button_stereo, master.button_measurement, master.button_anglemeasurement, master.button_cameratext, master.button_savescreen )):
            button.setText(buttontext[index])
            
        for comp in ((master.button_loadEcho, master.button_loadDir, master.button_rotate,  \
            master.button_stereo, master.button_measurement, master.button_anglemeasurement, master.button_cameratext, master.button_savescreen )):
            layout.addWidget(comp)
            
        self.setLayout(layout)
        
class PositionControlItems(QWidget):
    def __init__(self, master):
        self.master = master        
        super(PositionControlItems, self).__init__()

        nscales = 4
        master.scale_azimuth, master.scale_elevation, master.scale_roll = [QDial() for i in range(nscales-1)]
        master.scale_stereodepth = QScrollBar(Qt.Horizontal)
        label_azimuth, label_elevation, label_roll, label_stereodepth = [QLabel() for i in range(nscales)]
        master.button_zoomin, master.button_zoomout, master.button_resetcamera = [QPushButton() for i in range(3)]
        label_stereodepth = QLabel("Stereo depth")
        
        for button, buttontext in zip((master.button_zoomin, master.button_zoomout, master.button_resetcamera),("Zoom In", "Zoom Out", "Reset")):
            button.setText(buttontext)
        
        layout = QGridLayout()
        for index, label, labeltext in zip(range(nscales), (label_azimuth, label_elevation, label_roll), ("Azimuth", "Elevation", "Roll")):
            label.setText(labeltext)
            label.setAlignment(Qt.AlignRight)
        
        layout.addWidget(master.button_zoomin, 0, 7)
        layout.addWidget(master.button_zoomout, 0, 8)
        
        for index, scale in enumerate((master.scale_azimuth, master.scale_elevation, master.scale_roll)):
            scale.setMinimum(-179)
            scale.setMaximum(180)
            scale.setValue(0)
            scale.setMaximumSize(QSize(60,60))
            
        for index, comp in enumerate((label_azimuth, master.scale_azimuth,label_elevation,  master.scale_elevation, label_roll, master.scale_roll)):
            layout.addWidget(comp,0,index, 2, 1)
        
        layout.addWidget(master.button_resetcamera, 1, 8)
            
        master.scale_stereodepth.setValue(20)            
        master.scale_stereodepth.setMinimum(10)
        master.scale_stereodepth.setMaximum(100)
        layout.addWidget(label_stereodepth,0,6)            
        layout.addWidget(master.scale_stereodepth,1,6)
        
        layout.setMargin(0)
        layout.setHorizontalSpacing(20)       
        layout.setVerticalSpacing(0)     
        
        for col, val in enumerate((1,2,1,2,1,2,4,4,4)):
            layout.setColumnStretch(col,val)
                    
        self.setLayout(layout)
        
class TransferFunctionControlItems(QWidget):
    def __init__(self, master):
        super(TransferFunctionControlItems, self).__init__()
        self.button_edittransfunction, self.button_editopacity, self.button_editcolor, self.button_editgradient, self.button_savetfunction = [QPushButton() for i in range(5)]
        buttontext = ["Edit Transfer Function", "Edit Opacity", "Edit Color", "Edit Gradient Opacity", "Save Transfer Function"]        
     
        self.combobox_transfunction = QComboBox()
        label_transfunction = QLabel("Transfer Function")
        label_transfunction.setAlignment(Qt.AlignCenter)
     
        for index, button in enumerate((self.button_edittransfunction, self.button_editopacity, self.button_editcolor, self.button_editgradient, self.button_savetfunction)):
            button.setText(buttontext[index])
     
        layout = QHBoxLayout() 
        layout.setSpacing(0)
        for comp in ((label_transfunction, self.combobox_transfunction, self.button_edittransfunction, self.button_editopacity, self.button_editcolor, self.button_editgradient, self.button_savetfunction)):
            layout.addWidget(comp)
     
        self.setLayout(layout)

class vtkTimerCallBack():
    def __init__(self, K, isplay, isrotate, slider_imageNumber):
        self.timer_count = 0
        self.K = K
        self.isplay = isplay
        self.isrotate = isrotate
        self.slider_imageNumber = slider_imageNumber
        
    def execute(self, obj, event):
        iren = obj
        if self.isplay: 
            self.slider_imageNumber.setValue(self.timer_count % self.K)
            self.timer_count += 1
            
        if self.isrotate:
            self.renderer.GetActiveCamera().Azimuth(-1)  
            
        iren.GetRenderWindow().Render()
        
    def setplay(self, isplay):
        self.isplay = isplay
        
    def setrotate(self, isrotate):
        self.isrotate = isrotate         

class OpacityEditor(QDialog):
    def __init__(self, volumeProperty, reader, renWin):
        super(OpacityEditor, self).__init__()  
        self.volumeProperty0 = volumeProperty
        self.reader = reader
        self.renWin = renWin
        self.setWindowTitle("Opacity Editor") 
        self.opacityfunction =  self.volumeProperty0.GetScalarOpacity(0)
        self.npts = self.opacityfunction.GetSize()
        
        self.vScale = [[QSlider(Qt.Horizontal) for i in range(4)] for j in range(self.npts)]  
        self.label_value = [[QLabel(" ") for i in range(4)] for j in range(self.npts)]  
        
        label_scaleName = [QLabel() for j in range(4)]
        for j, text in enumerate(("Intensity", "Opacity","Midpoint","Sharpness")):
            label_scaleName[j].setText(text)       
        
        layout = QGridLayout()
        
        for j in range(4):
            layout.addWidget(label_scaleName[j],0,2*j)
        
        rmax = self.reader.GetOutput().GetScalarRange()[1] if self.reader.GetOutput().GetScalarRange()[1]> self.opacityfunction.GetRange()[1] else self.opacityfunction.GetRange()[1]
        rmin = self.reader.GetOutput().GetScalarRange()[0] if self.reader.GetOutput().GetScalarRange()[0] < self.opacityfunction.GetRange()[0] else self.opacityfunction.GetRange()[0]
        opacityNode = np.empty((4,))

        for i in range(self.npts):
            self.opacityfunction.GetNodeValue(i, opacityNode)
            for j in range(4):
                layout.addWidget(self.label_value[i][j],2*i,2*j+1)
                layout.addWidget(self.vScale[i][j],2*i+1,2*j,1,2)  
                if j==0:
                    self.vScale[i][j].setMinimum(rmin)
                    self.vScale[i][j].setMaximum(rmax)
                    self.vScale[i][j].setValue(opacityNode[j])
                else:        
                    self.vScale[i][j].setValue(100*opacityNode[j])

                self.vScale[i][j].valueChanged.connect(self.updateOpacity)
        
        self.updateOpacity()
        
        layout.setSpacing(0)
        layout.setHorizontalSpacing(10)
        self.setLayout(layout)
        self.resize(400,50*self.npts)
        
    def updateOpacity(self):
        self.opacityfunction.RemoveAllPoints()
        for i in range(self.npts):
            self.opacityfunction.AddPoint(self.vScale[i][0].value(),0.01*self.vScale[i][1].value(),0.01*self.vScale[i][2].value(),0.01*self.vScale[i][3].value())

            for j in range(4):
                if j == 0:
                    self.label_value[i][j].setText(str(self.vScale[i][j].value()))
                else:
                    self.label_value[i][j].setText(str(0.01*self.vScale[i][j].value()))
            
        self.volumeProperty0.SetScalarOpacity(self.opacityfunction)
        self.renWin.Render()            

class GradientOpacityEditor(QDialog):
    def __init__(self, volumeProperty, reader, renWin):
        super(GradientOpacityEditor, self).__init__()  
        self.volumeProperty0 = volumeProperty
        self.reader = reader
        self.renWin = renWin
        self.setWindowTitle("Gradient Opacity Editor") 
        self.opacityfunction =  self.volumeProperty0.GetGradientOpacity(0)
        self.npts = self.opacityfunction.GetSize()
        
        self.vScale = [[QSlider(Qt.Horizontal) for i in range(4)] for j in range(self.npts)]  
        self.label_value = [[QLabel(" ") for i in range(4)] for j in range(self.npts)]  
        
        label_scaleName = [QLabel() for j in range(4)]
        for j, text in enumerate(("Intensity", " Opacity","Midpoint","Sharpness")):
            label_scaleName[j].setText(text)       
        
        layout = QGridLayout()
        
        for j in range(4):
            layout.addWidget(label_scaleName[j],0,2*j)
        
        rmax = self.reader.GetOutput().GetScalarRange()[1] if self.reader.GetOutput().GetScalarRange()[1]> self.opacityfunction.GetRange()[1] else self.opacityfunction.GetRange()[1]
        rmin = self.reader.GetOutput().GetScalarRange()[0] if self.reader.GetOutput().GetScalarRange()[0] < self.opacityfunction.GetRange()[0] else self.opacityfunction.GetRange()[0]
        opacityNode = np.empty((4,))

        for i in range(self.npts):
            self.opacityfunction.GetNodeValue(i, opacityNode)
            for j in range(4):
                layout.addWidget(self.label_value[i][j],2*i,2*j+1)
                layout.addWidget(self.vScale[i][j],2*i+1,2*j,1,2)  
                if j==0:
                    self.vScale[i][j].setMinimum(rmin)
                    self.vScale[i][j].setMaximum(rmax)
                    self.vScale[i][j].setValue(opacityNode[j])
                else:        
                    self.vScale[i][j].setValue(100*opacityNode[j])

                self.vScale[i][j].valueChanged.connect(self.updateOpacity)
        
        self.updateOpacity()
        
        layout.setSpacing(0)
        layout.setHorizontalSpacing(10)
        self.setLayout(layout)
        self.resize(400,50*self.npts)
        
    def updateOpacity(self):
        self.opacityfunction.RemoveAllPoints()
        for i in range(self.npts):
            self.opacityfunction.AddPoint(self.vScale[i][0].value(),0.01*self.vScale[i][1].value(),0.01*self.vScale[i][2].value(),0.01*self.vScale[i][3].value())

            for j in range(4):
                if j == 0:
                    self.label_value[i][j].setText(str(self.vScale[i][j].value()))
                else:
                    self.label_value[i][j].setText(str(0.01*self.vScale[i][j].value()))
            
        self.volumeProperty0.SetGradientOpacity(self.opacityfunction)
        self.renWin.Render()   
        
class ColorEditor(QDialog):        
    def __init__(self, volumeProperty, reader, renWin):
        super(ColorEditor, self).__init__()  
        self.volumeProperty0 = volumeProperty
        self.reader = reader
        self.renWin = renWin
        self.setWindowTitle("Color Editor") 
        
        self.colorfunction =  self.volumeProperty0.GetRGBTransferFunction(0)
        self.npts = self.colorfunction.GetSize() 
        
        self.vScale = [[QSlider(Qt.Horizontal) for i in range(6)] for j in range(self.npts)]  
        self.label_value = [[QLabel(" ") for i in range(6)] for j in range(self.npts)]  
        
        label_scaleName = [QLabel() for j in range(6)]
        for j, text in enumerate(("Intensity", "Red","Green","Blue","Midpoint","Sharpness")):
            label_scaleName[j].setText(text)       
        
        layout = QGridLayout()
        
        for j in range(6):
            layout.addWidget(label_scaleName[j],0,2*j)
        
        rmax = self.reader.GetOutput().GetScalarRange()[1] if self.reader.GetOutput().GetScalarRange()[1]> self.colorfunction.GetRange()[1] else self.colorfunction.GetRange()[1]
        rmin = self.reader.GetOutput().GetScalarRange()[0] if self.reader.GetOutput().GetScalarRange()[0] < self.colorfunction.GetRange()[0] else self.colorfunction.GetRange()[0]
        opacityNode = np.empty((6,))

        for i in range(self.npts):
            self.colorfunction.GetNodeValue(i, opacityNode)
            for j in range(6):
                layout.addWidget(self.label_value[i][j],2*i,2*j+1)
                layout.addWidget(self.vScale[i][j],2*i+1,2*j,1,2)  
                if j==0:
                    self.vScale[i][j].setMinimum(rmin)
                    self.vScale[i][j].setMaximum(rmax)
                    self.vScale[i][j].setValue(opacityNode[j])
                else:        
                    self.vScale[i][j].setValue(100*opacityNode[j])

                self.vScale[i][j].valueChanged.connect(self.updateColor)
        
        self.updateColor()
        
        layout.setSpacing(0)
        layout.setHorizontalSpacing(10)
        self.setLayout(layout)
        self.resize(600,50*self.npts)
        
    def updateColor(self):
        self.colorfunction.RemoveAllPoints()
        for i in range(self.npts):
            self.colorfunction.AddRGBPoint(self.vScale[i][0].value(),0.01*self.vScale[i][1].value(),0.01*self.vScale[i][2].value(),0.01*self.vScale[i][3].value(),0.01*self.vScale[i][4].value(),0.01*self.vScale[i][5].value())

            for j in range(6):
                if j == 0:
                    self.label_value[i][j].setText(str(self.vScale[i][j].value()))
                else:
                    self.label_value[i][j].setText(str(0.01*self.vScale[i][j].value()))
            
        self.volumeProperty0.SetColor(self.colorfunction)
        self.renWin.Render()         
               
class TransferFunctionEditor(pysideQWidget): 
    def __init__(self, volumeProperty, reader, renWin):        
        super(TransferFunctionEditor, self).__init__()    
        self.volumeProperty0 = volumeProperty
        self.reader = reader
        self._renderWindow = renWin
        self.setWindowTitle("Transfer Function Editor") 
        
        self.opacityfunction =  self.volumeProperty0.GetScalarOpacity(0)
        self.colorfunction =  self.volumeProperty0.GetRGBTransferFunction(0)

        self.npts = self.opacityfunction.GetSize()
        
        imageData = reader.GetOutput()
        self.histogramWidget = TransferFunctionWidget()
        transferFunction = TransferFunction()
        
        rmax = self.reader.GetOutput().GetScalarRange()[1] 
        rmin = self.reader.GetOutput().GetScalarRange()[0]         
        transferFunction.setRange([rmin, rmax])
        
        self.minimum, self.maximum = rmin, rmax
        
        opacityNode = np.empty((4,))
        transferFunction.addPoint(rmin, self.opacityfunction.GetValue(rmin), color=[self.colorfunction.GetRedValue(rmin), self.colorfunction.GetGreenValue(rmin), self.colorfunction.GetBlueValue(rmin)])
        for i in range(self.npts):
            self.opacityfunction.GetNodeValue(i, opacityNode)
            if (opacityNode[0] > rmin) and (opacityNode[0] < rmax):
                transferFunction.addPoint(opacityNode[0], opacityNode[1], color=[self.colorfunction.GetRedValue(opacityNode[0]), self.colorfunction.GetGreenValue(opacityNode[0]), self.colorfunction.GetBlueValue(opacityNode[0])])       
            
        transferFunction.addPoint(rmax, self.opacityfunction.GetValue(rmax), color=[self.colorfunction.GetRedValue(rmax), self.colorfunction.GetGreenValue(rmax), self.colorfunction.GetBlueValue(rmax)])
        
        transferFunction.updateTransferFunction() 
        
        self.histogramWidget.transferFunction = transferFunction 
        self.histogramWidget.setImageData(imageData)        
        self.histogramWidget.transferFunction.updateTransferFunction()        
        self.histogramWidget.updateNodes()  
        
        self.histogramWidget.valueChanged.connect(self.valueChanged)

        self.resize(400,300)
        
    def getTransferFunctionWidget(self):
        return self.histogramWidget        
    
    def updateTransferFunction(self):
        if self.histogramWidget:
            self.histogramWidget.transferFunction.updateTransferFunction()
            self.colorFunction = self.histogramWidget.transferFunction.colorFunction
            self.opacityFunction = self.histogramWidget.transferFunction.opacityFunction
        else:
            # Transfer functions and properties
            self.colorFunction = vtkColorTransferFunction()
            self.colorFunction.AddRGBPoint(self.minimum, 0, 0, 0)
            self.colorFunction.AddRGBPoint(self.maximum, 0, 0, 0)

            self.opacityFunction = vtkPiecewiseFunction()
            self.opacityFunction.AddPoint(self.minimum, 0)
            self.opacityFunction.AddPoint(self.maximum, 0)

        self.volumeProperty0.SetColor(self.colorFunction)
        self.volumeProperty0.SetScalarOpacity(self.opacityFunction)

        self._renderWindow.Render() 
        
    def valueChanged(self, value):
        self.updateTransferFunction()
        
class TDVizCustom(TDViz):      
    def __init__(self, parent=None):
        TDViz.__init__(self, parent)
        
        self._renWin = self.vtkWidget.GetRenderWindow()
        self._renWin.StereoCapableWindowOn()
        self._renWin.SetStereoTypeToCrystalEyes()
        self._renWin.StereoRenderOn()  

        self.volume0 = vtk.vtkVolume()
        self.volumeProperty0 = vtk.vtkVolumeProperty()
        transform1 = vtk.vtkTransform()
        self.volume0.SetUserTransform(transform1)
        
        self.volume1 = vtk.vtkVolume()
        self.volumeProperty1 = vtk.vtkVolumeProperty()
        transform2 = vtk.vtkTransform()
        self.volume1.SetUserTransform(transform2)
        
        self._ren = vtk.vtkRenderer()
        self._renWin.AddRenderer(self._ren)
        self._iren = self._renWin.GetInteractor()  
        self._iren.SetInteractorStyle(vtk.vtkInteractorStyleTrackballActor())
        self.removeAllMouseEvents(self._iren)
        
        self.sopuid = []
        self.reader = []

        self.initAxes()  
#         self.initBoxWidget()
        self.initMeasurementTool()
        self.initAngleMeasurementTool()
        self.initPlaneWidget()
        self.initSphereWidget()
        self.initLabels()
        self.initSplitPlaneWidget()
      
        tfuncs = glob.glob1(tfuncdir, "*.vvt")
        self.transferFunctionControlItems.combobox_transfunction.addItems(tfuncs)        

        self.varscaleazimuth = self.scale_azimuth.value()
        self.varscaleelevation = self.scale_elevation.value()

        self._ren.GetActiveCamera().AddObserver("AnyEvent", self.cameraAnyEvent)
        
        self.imageGaussianSmooth = vtk.vtkImageGaussianSmooth()
        
        self._renWin.Render()  

        self.camera = self._ren.GetActiveCamera()
        self.camera.UseOffAxisProjectionOn()  

        self.camera.SetUseOffAxisProjection(1)
        self.camera.SetEyeSeparation(60)        

        self.initHeadTrackText()
        
        self._renWin.Render()  
        
    def removeAllMouseEvents(self, obj):       
        obj.RemoveObservers('LeftButtonPressEvent')
        obj.RemoveObservers('RightButtonPressEvent')
        obj.RemoveObservers('ButtonPressEvent')
        obj.RemoveObservers('LeftButtonReleaseEvent')
        obj.RemoveObservers('RightButtonReleaseEvent')
        obj.RemoveObservers('ButtonReleaseEvent')
        obj.RemoveObservers('MouseMoveEvent')
        obj.RemoveObservers('MouseWheelForwardEvent')
        obj.RemoveObservers('MouseWheelBackwardEvent')
        obj.RemoveObservers('MouseMoveEvent')        
        
    def initHeadTrackText(self):
        self.headtracktext = vtk.vtkTextActor()        
        self.headtracktext.SetDisplayPosition(20, 20)      
        self.headtracktext.SetInput("0")
        self._ren.AddActor(self.headtracktext)  

        self.stylustext = vtk.vtkTextActor()        
        self.stylustext.SetDisplayPosition(1600, 20)      
        self.stylustext.SetInput("0")
        self._ren.AddActor(self.stylustext)
        
    def initGrid(self):
        sourceX = vtk.vtkLineSource() 
        sourceX.SetResolution(30)
        sourceX.SetPoint1(0.0, 0.0, 0.0)
        sourceX.SetPoint2(100.0, 0.0, 0.0) 
        mapperX = vtk.vtkPolyDataMapper()
        mapperX.SetInputConnection(sourceX.GetOutputPort())
        self.xAxisActor = vtk.vtkActor()
        self.xAxisActor.SetMapper(mapperX)
        self.xAxisActor.GetProperty().SetColor(1.0,0.0,0.0)
        
        sourceY = vtk.vtkLineSource() 
        sourceY.SetResolution(30)
        sourceY.SetPoint1(0.0, 0.0, 0.0)
        sourceY.SetPoint2(0.0, 100.0, 0.0) 
        mapperY = vtk.vtkPolyDataMapper()
        mapperY.SetInputConnection(sourceY.GetOutputPort())
        self.yAxisActor = vtk.vtkActor()
        self.yAxisActor.SetMapper(mapperY)
        self.yAxisActor.GetProperty().SetColor(0.0,1.0,0.0)
        
        sourceZ = vtk.vtkLineSource() 
        sourceZ.SetResolution(30)
        sourceZ.SetPoint1(0.0, 0.0, 0.0)
        sourceZ.SetPoint2(0.0, 0.0, 100.0) 
        mapperZ = vtk.vtkPolyDataMapper()
        mapperZ.SetInputConnection(sourceZ.GetOutputPort())
        self.zAxisActor = vtk.vtkActor()
        self.zAxisActor.SetMapper(mapperZ)
        self.zAxisActor.GetProperty().SetColor(0.0,0.0,100.0)
        
        self._ren.AddActor(self.xAxisActor)
        self._ren.AddActor(self.yAxisActor)
        self._ren.AddActor(self.zAxisActor)
        
    def initStylus(self):
        sphere = vtk.vtkSphereSource()
        sphere.SetThetaResolution(30)
        sphere.SetPhiResolution(30)
        sphere.SetRadius(2)

        sphereTransform = vtk.vtkTransform()
        sphereTransform.Translate(0, 0, -100)        
        
        sphereTransformFilter = vtk.vtkTransformPolyDataFilter()
        sphereTransformFilter.SetInputConnection(sphere.GetOutputPort())
        sphereTransformFilter.SetTransform(sphereTransform)
        
        line = vtk.vtkLineSource()
        line.SetResolution(30)
        line.SetPoint1(0.0, 0.0, 0.0)
        line.SetPoint2(0.0, 0.0, -100)
        
        appendFilter = vtk.vtkAppendPolyData()
        appendFilter.AddInputConnection(line.GetOutputPort())
        appendFilter.AddInputConnection(sphereTransformFilter.GetOutputPort())
        
        lineMapper = vtk.vtkPolyDataMapper()
        lineMapper.SetInputConnection(appendFilter.GetOutputPort())
        
        self.cursor = vtk.vtkActor()
        transform = vtk.vtkTransform()
        self.cursor.SetUserTransform(transform)
        self.cursor.SetMapper(lineMapper)    
        
        self._ren.AddActor(self.cursor)
    
    def initAxes(self):
        self.axes = vtk.vtkAxesActor()
        self.axesWidget = vtk.vtkOrientationMarkerWidget()
        self.axes.AxisLabelsOff()
        self.axes.SetShaftTypeToCylinder()
        
        self.axesWidget.SetOrientationMarker(self.axes)
        self.axesWidget.SetInteractor(self._iren)
        self.axesWidget.SetViewport(0.0, 0.0, 0.15, 0.15)
        self.axesWidget.SetEnabled(1)
        self.axesWidget.InteractiveOff()   
             
        self.cameraText = vtk.vtkTextActor()        
        self.cameraText.SetTextScaleModeToProp()
        self.cameraText.SetDisplayPosition(10, 10)
        
#     def initBoxWidget(self):
#         self.boxWidget = vtk.vtkBoxWidget()
#         self.boxWidget.SetInteractor(self._iren)
#         self.boxWidget.SetPlaceFactor(1.0)
#         self.boxWidget.SetHandleSize(0.004)
#         self.boxWidget.TranslationEnabledOff()
#         self.boxWidget.InsideOutOn()
#         self.boxWidget.AddObserver("StartInteractionEvent", self.bwStartInteraction)
#         self.boxWidget.AddObserver("InteractionEvent", self.bwClipVolumeRender)
#         self.boxWidget.AddObserver("EndInteractionEvent", self.bwEndInteraction) 
#         self.planes = vtk.vtkPlanes()
        
    def initSplitPlaneWidget(self):
        self.splitPlaneWidget0 = vtk.vtkImplicitPlaneWidget()
        self.splitPlaneWidget0.SetInteractor(self._iren)
        self.splitPlaneWidget0.SetPlaceFactor(1.5)
        self.splitPlaneWidget0.TubingOff()
        self.splitPlaneWidget0.GetOutlineProperty().SetOpacity(0.0)
        self.splitPlaneWidget0.GetPlaneProperty().SetOpacity(1.0)
        self.splitPlaneWidget0.GetPlaneProperty().SetColor(0.0,0.5,1.0)
        self.splitPlaneWidget0.GetNormalProperty().SetOpacity(0.0)
        self.splitPlaneWidget0.OutlineTranslationOff() 
        self.splitPlaneWidget0.ScaleEnabledOff()
        self.splitPlaneWidget0.SetHandleSize(0.004)
        
        self.splitPlaneWidget1 = vtk.vtkImplicitPlaneWidget()
        self.splitPlaneWidget1.SetInteractor(self._iren)
        self.splitPlaneWidget1.SetPlaceFactor(1.5)
        self.splitPlaneWidget1.TubingOff()
        self.splitPlaneWidget1.GetOutlineProperty().SetOpacity(0.0)
        self.splitPlaneWidget1.GetPlaneProperty().SetOpacity(0.5)
        self.splitPlaneWidget1.GetPlaneProperty().SetColor(0.0,0.5,1.0)
        self.splitPlaneWidget1.GetNormalProperty().SetOpacity(0.0)
        self.splitPlaneWidget1.OutlineTranslationOff() 
        self.splitPlaneWidget1.ScaleEnabledOff()
        self.splitPlaneWidget1.SetHandleSize(0.004)
        
        self.spwPlane0 = vtk.vtkPlane()
        self.spwPlane1 = vtk.vtkPlane()
        self.spwClippingPlanes0 = vtk.vtkPlaneCollection()
        self.spwClippingPlanes1 = vtk.vtkPlaneCollection()
        
    def initPlaneWidget(self):
        self.planeWidget = [vtk.vtkImplicitPlaneWidget() for i in range(6)]
        self.pwPlane = [vtk.vtkPlane() for i in range(6)]
        self.pwClippingPlanes = vtk.vtkPlaneCollection()
              
        for i in range(6):
            self.planeWidget[i].SetInteractor(self._iren)
            self.planeWidget[i].SetPlaceFactor(1.1)
            self.planeWidget[i].TubingOff()
            self.planeWidget[i].DrawPlaneOff()
#             self.planeWidget[i].OutsideBoundsOff()
            self.planeWidget[i].OutsideBoundsOn()  
            self.planeWidget[i].OutlineTranslationOff() 
            self.planeWidget[i].ScaleEnabledOff()
            self.planeWidget[i].SetHandleSize(0.25*self.planeWidget[i].GetHandleSize())
            self.planeWidget[i].SetKeyPressActivationValue(str(i+1))
            self.planeWidget[i].AddObserver("InteractionEvent", self.pwCallback)  
            self.pwClippingPlanes.AddItem(self.pwPlane[i])
            
    def initSphereWidget(self):
        self.lightkit = vtk.vtkLightKit()
        self.lightkit.SetKeyLightIntensity(2.0)
        self.lightkit.MaintainLuminanceOn()
        self.lightkit.AddLightsToRenderer(self._ren)
        
    def initLabels(self):
        nlabels = self.labelControlItems.combobox_labels.count()
        self.labelText = [vtk.vtkVectorText() for i in range(nlabels)]
        self.labelMapper = [vtk.vtkPolyDataMapper() for i in range(nlabels)]
        self.labelActor = [vtk.vtkFollower() for i in range(nlabels)]
        self.labelLine = [vtk.vtkLineWidget2() for i in range(nlabels)]
        self.labelLineRep = [vtk.vtkLineRepresentation() for i in range(nlabels)]
        
        for i in range(nlabels):            
            self.labelText[i].SetText("Label"+str(i+1))
            
            self.labelMapper[i].SetInputConnection(self.labelText[i].GetOutputPort())
            
            self.labelActor[i].SetMapper(self.labelMapper[i])
            self.labelActor[i].SetScale(2.0)            
            self.labelActor[i].GetProperty().SetColor(1,0,0) 
            
            self.labelLineRep[i].SetHandleSize(0)
            
            self.labelLine[i].SetRepresentation(self.labelLineRep[i])            
            self.labelLine[i].SetInteractor(self._iren)
            self.labelLine[i].AddObserver("StartInteractionEvent", self.labelStartInteraction)
            self.labelLine[i].AddObserver("InteractionEvent", self.labelEndInteraction)        
            
            
            self.labelActor[i].SetCamera(self._ren.GetActiveCamera())
            
    def initMeasurementTool(self):
        self.distanceRep = vtk.vtkDistanceRepresentation3D()
        self.distanceRep.SetLabelFormat("%-#6.3g mm")
        
        self.distanceWidget = vtk.vtkDistanceWidget()
        self.distanceWidget.SetInteractor(self._iren)
        self.distanceWidget.SetWidgetStateToManipulate()
        #self.distanceWidget.CreateDefaultRepresentation()
        self.distanceWidget.SetRepresentation(self.distanceRep)
        self.distanceWidget.SetEnabled(0)  
        self.removeAllMouseEvents(self.distanceWidget)
        
        self.distanceWidget.AddObserver("StartInteractionEvent", self.dwStartInteraction)
        self.distanceWidget.AddObserver("InteractionEvent", self.dwUpdateMeasurement)
        self.distanceWidget.AddObserver("EndInteractionEvent", self.dwEndInteraction)   
        
        self.distanceText = vtk.vtkTextActor()        
        self.distanceText.SetDisplayPosition(900, 10)
        self.distanceText.SetInput("distance = ")    

    def initAngleMeasurementTool(self):
        self.angleRep = vtk.vtkAngleRepresentation3D()
        
        self.angleWidget = vtk.vtkAngleWidget()
        self.angleWidget.SetInteractor(self._iren)
        self.angleWidget.SetWidgetStateToManipulate()
        self.angleWidget.CreateDefaultRepresentation()
        self.angleWidget.SetRepresentation(self.angleRep)
        self.angleWidget.SetEnabled(0)
        
        self.angleWidget.AddObserver("StartInteractionEvent", self.awStartInteraction)
        self.angleWidget.AddObserver("InteractionEvent", self.awUpdateMeasurement)
        self.angleWidget.AddObserver("EndInteractionEvent", self.awEndInteraction)   

        self.angleText = vtk.vtkTextActor()        
        self.angleText.SetDisplayPosition(900, 20)  

        self.angleText.SetInput("angle = ")   
            
    def resetAllPlaneWidgets(self):
        if self.reader:
            xmin, xmax, ymin, ymax, zmin, zmax = self.reader.GetOutput().GetBounds()
                
            self.planeWidget[0].SetOrigin(0,ymax/2.,zmax/2.)
            self.planeWidget[0].SetNormal(1,0,0)
            self.planeWidget[0].UpdatePlacement() 
    
            self.planeWidget[1].SetOrigin(xmax/2.,0,zmax/2.)
            self.planeWidget[1].SetNormal(0,1,0)
            self.planeWidget[1].UpdatePlacement() 
    
            self.planeWidget[2].SetOrigin(xmax/2.,ymax/2.,0)
            self.planeWidget[2].SetNormal(0,0,1)
            self.planeWidget[2].UpdatePlacement() 
            
    
            self.planeWidget[3].SetOrigin(xmax,ymax/2.,zmax/2.)
            self.planeWidget[3].SetNormal(-1,0,0)
            self.planeWidget[3].UpdatePlacement() 
    
            self.planeWidget[4].SetOrigin(xmax/2.,ymax,zmax/2.)
            self.planeWidget[4].SetNormal(0,-1,0)
            self.planeWidget[4].UpdatePlacement() 
    
            self.planeWidget[5].SetOrigin(xmax/2.,ymax/2.,zmax)
            self.planeWidget[5].SetNormal(0,0,-1)
            self.planeWidget[5].UpdatePlacement() 
                
            self.pwCallback(self, None)
            self._renWin.Render()
            
    def resetPlaneWidget(self):
        if self.reader:
            
            sendername = self.sender().objectName()            
            
            xmin, xmax, ymin, ymax, zmin, zmax = self.reader.GetOutput().GetBounds()
            
            if sendername == "resetplane0":                
                self.planeWidget[0].SetOrigin(0,ymax/2.,zmax/2.)
                self.planeWidget[0].SetNormal(1,0,0)
                self.planeWidget[0].UpdatePlacement() 
            elif sendername == "resetplane1":
                self.planeWidget[1].SetOrigin(xmax/2.,0,zmax/2.)
                self.planeWidget[1].SetNormal(0,1,0)
                self.planeWidget[1].UpdatePlacement() 
            elif sendername == "resetplane2":        
                self.planeWidget[2].SetOrigin(xmax/2.,ymax/2.,0)
                self.planeWidget[2].SetNormal(0,0,1)
                self.planeWidget[2].UpdatePlacement() 
            elif sendername == "resetplane3":
                self.planeWidget[3].SetOrigin(xmax,ymax/2.,zmax/2.)
                self.planeWidget[3].SetNormal(-1,0,0)
                self.planeWidget[3].UpdatePlacement() 
            elif sendername == "resetplane4":
                self.planeWidget[4].SetOrigin(xmax/2.,ymax,zmax/2.)
                self.planeWidget[4].SetNormal(0,-1,0)
                self.planeWidget[4].UpdatePlacement() 
            elif sendername == "resetplane5":        
                self.planeWidget[5].SetOrigin(xmax/2.,ymax/2.,zmax)
                self.planeWidget[5].SetNormal(0,0,-1)
                self.planeWidget[5].UpdatePlacement() 
                
            self.pwCallback(self, None)
            self._renWin.Render()
            
    def resetCrop(self):
        for scale, val in zip((self.scale_xmin,self.scale_ymin,self.scale_zmin,self.scale_xmax,self.scale_ymax,self.scale_zmax),(0,0,0,self.dim[0],self.dim[1],self.dim[2])):
            scale.setValue(val)
        self.cropVolume()
        self.resetBoxWidget()
        self._renWin.Render()
            
#     def resetBoxWidget(self):
#         self.volumeMapper0.RemoveAllClippingPlanes()
#         self.boxWidget.PlaceWidget()
        
    def labelStartInteraction(self, obj,evt):
        nlabels = self.labelControlItems.combobox_labels.count()
        for i in range(nlabels):
            self.labelActor[i].SetPosition(self.labelLine[i].GetLineRepresentation().GetPoint2WorldPosition())
        
    def labelEndInteraction(self, obj,evt):
        nlabels = self.labelControlItems.combobox_labels.count()
        for i in range(nlabels):
            self.labelActor[i].SetPosition(self.labelLine[i].GetLineRepresentation().GetPoint2WorldPosition())
            
    def pwCallback(self, obj, evt):
        self.volumeMapper0.RemoveAllClippingPlanes()
#         self.pwClippingPlanes.RemoveAllItems()

        for i in range(6):
            self.planeWidget[i].GetPlane(self.pwPlane[i])
            self.pwClippingPlanes.AddItem(self.pwPlane[i])   
            
#         self.volumeMapper0.SetClippingPlanes(self.pwClippingPlanes)
       
    def spwPlane0Callback(self):
        volMat = self.VTKtoNP4x4(self.volume0.GetMatrix())
        
        origin = np.ones((4,1))
        origin[0],origin[1],origin[2] = self.splitPlaneWidget0.GetOrigin()[0],self.splitPlaneWidget0.GetOrigin()[1],self.splitPlaneWidget0.GetOrigin()[2]
        self.spwOriginR0 = np.linalg.inv(volMat).dot(origin)
        
        volMat[0][3], volMat[1][3], volMat[2][3] = 0.0, 0.0, 0.0
        
        normal = np.ones((4,1))
        normal[0],normal[1],normal[2] = self.splitPlaneWidget0.GetNormal()[0],self.splitPlaneWidget0.GetNormal()[1],self.splitPlaneWidget0.GetNormal()[2]
        self.spwNormalR0 = np.linalg.inv(volMat).dot(normal)
        
    def spwPlane1Callback(self):
        volMat = self.VTKtoNP4x4(self.volume1.GetMatrix())
        
        origin = np.ones((4,1))
        origin[0],origin[1],origin[2] = self.splitPlaneWidget1.GetOrigin()[0],self.splitPlaneWidget1.GetOrigin()[1],self.splitPlaneWidget1.GetOrigin()[2]
        self.spwOriginR1 = np.linalg.inv(volMat).dot(origin)
        
        volMat[0][3], volMat[1][3], volMat[2][3] = 0.0, 0.0, 0.0
        
        normal = np.ones((4,1))
        normal[0],normal[1],normal[2] = self.splitPlaneWidget1.GetNormal()[0],self.splitPlaneWidget1.GetNormal()[1],self.splitPlaneWidget1.GetNormal()[2]
        self.spwNormalR1 = np.linalg.inv(volMat).dot(normal)
        
        
        #called when labelLine changes, saves relative position
    def labelCallback(self):

        transform = vtk.vtkTransform()
        transform.Concatenate(self.volume.GetMatrix())
        nlabels = self.labelControlItems.combobox_labels.count()
        labelLocation1A = [np.empty((3)) for i in range(nlabels)]
        labelLocation2A = [np.empty((3)) for i in range(nlabels)]
        labelLocation3A = [np.empty((3)) for i in range(nlabels)]
        self.labelLocation1R = [np.empty((3)) for i in range(nlabels)]
        self.labelLocation2R = [np.empty((3)) for i in range(nlabels)]
        self.labelLocation3R = [np.empty((3)) for i in range(nlabels)]
        for i in range(nlabels):
            for j in range(3):
                labelLocation1A[i][j] = self.labelLine[i].GetLineRepresentation().GetPoint1WorldPosition()[j]
                labelLocation2A[i][j] = self.labelLine[i].GetLineRepresentation().GetPoint2WorldPosition()[j]
                labelLocation3A[i][j] = self.labelActor[i].GetPosition()[j]
            transform.GetInverse().TransformPoint(labelLocation1A[i], self.labelLocation1R[i])
            transform.GetInverse().TransformPoint(labelLocation2A[i], self.labelLocation2R[i])
            transform.GetInverse().TransformPoint(labelLocation3A[i], self.labelLocation3R[i])
        
    #called when volume changes, sets labelLine absolute position based on relative position
    def labelUpdate(self):

        transform = vtk.vtkTransform()
        transform.Concatenate(self.volume.GetMatrix())
        nlabels = self.labelControlItems.combobox_labels.count()
        labelLocation1A = [np.empty((3)) for i in range(nlabels)]
        labelLocation2A = [np.empty((3)) for i in range(nlabels)]
        labelLocation3A = [np.empty((3)) for i in range(nlabels)]
        for i in range(nlabels):
            transform.TransformPoint(self.labelLocation1R[i], labelLocation1A[i])
            transform.TransformPoint(self.labelLocation2R[i], labelLocation2A[i])
            transform.TransformPoint(self.labelLocation3R[i], labelLocation3A[i])
            self.labelLine[i].GetLineRepresentation().SetPoint1WorldPosition(labelLocation1A[i])
            self.labelLine[i].GetLineRepresentation().SetPoint2WorldPosition(labelLocation2A[i])
            self.labelActor[i].SetPosition(labelLocation3A[i])
        self.labelBeginInteraction(None, None)
        
    def spwUpdate0(self, volMat):
        spwOriginA = volMat.dot(self.spwOriginR0)
        volMat[0][3], volMat[1][3], volMat[2][3] = 0.0, 0.0, 0.0
        spwNormalA = volMat.dot(self.spwNormalR0)
           
        self.splitPlaneWidget0.SetOrigin(spwOriginA[0],spwOriginA[1],spwOriginA[2])
        self.splitPlaneWidget0.SetNormal(spwNormalA[0],spwNormalA[1],spwNormalA[2])
        self.splitPlaneWidget0.Modified()
        
    def spwUpdate1(self, volMat):
        spwOriginA = volMat.dot(self.spwOriginR1)
        volMat[0][3], volMat[1][3], volMat[2][3] = 0.0, 0.0, 0.0
        spwNormalA = volMat.dot(self.spwNormalR1)
           
        self.splitPlaneWidget1.SetOrigin(spwOriginA[0],spwOriginA[1],spwOriginA[2])
        self.splitPlaneWidget1.SetNormal(spwNormalA[0],spwNormalA[1],spwNormalA[2])
        self.splitPlaneWidget1.Modified()   
        
    def applySplitClip0(self):
        self.splitPlaneWidget0.GetPlane(self.spwPlane0)
        self.volumeMapper0.AddClippingPlane(self.spwPlane0) 
        
    def applySplitClip1(self):
        self.splitPlaneWidget1.GetPlane(self.spwPlane1)
        self.spwPlane1.SetNormal(-self.spwPlane1.GetNormal()[0], -self.spwPlane1.GetNormal()[1], -self.spwPlane1.GetNormal()[2])
        self.volumeMapper1.AddClippingPlane(self.spwPlane1) 
        
    def volumeCallback0(self, obj, event, userData = None):
        self.splitPlaneWidget0.PlaceWidget()
        for i in range(6): self.planeWidget[i].PlaceWidget()
            
        volMat = self.VTKtoNP4x4(self.volume0.GetMatrix())
        self.spwUpdate0(volMat)
           
        self.volumeMapper0.RemoveAllClippingPlanes()
        if self.volume1Enabled:
             self.applySplitClip0()
        
    def volumeCallback1(self, obj, event, userData = None):
        self.splitPlaneWidget1.PlaceWidget()
        volMat = self.VTKtoNP4x4(self.volume1.GetMatrix())
        self.spwUpdate1(volMat)
        
        self.volumeMapper1.RemoveAllClippingPlanes()
        if self.volume1Enabled:
            self.applySplitClip1()
            
     
    def dwStartInteraction(self, obj, event):
        self._renWin.SetDesiredUpdateRate(10)
    
    def dwEndInteraction(self, obj, event):
        #self.distanceText.SetInput("distance = %-#6.3g mm" % obj.GetDistanceRepresentation().GetDistance())        
        self._renWin.SetDesiredUpdateRate(0.001)        
        
    def dwUpdateMeasurement(self, obj, event):
        self.distanceText.SetInput("distance = %-#6.3g mm" % obj.GetDistanceRepresentation().GetDistance())  

    def awStartInteraction(self, obj, event):
        self._renWin.SetDesiredUpdateRate(10)
    
    def awEndInteraction(self, obj, event):
        self.angleText.SetInput("angle = %-#6.3g degrees" % vtk.vtkMath.DegreesFromRadians(obj.GetAngleRepresentation().GetAngle()))        
        self._renWin.SetDesiredUpdateRate(0.001)        
        
    def awUpdateMeasurement(self, obj, event):
        self.angleText.SetInput("angle = %-#6.3g degrees" % vtk.vtkMath.DegreesFromRadians(obj.GetAngleRepresentation().GetAngle()))     
              
#     def bwStartInteraction(self, obj, event):
#         self._renWin.SetDesiredUpdateRate(10)
#     
#     def bwEndInteraction(self, obj, event):
#         self._renWin.SetDesiredUpdateRate(0.001)
#     
#     def bwClipVolumeRender(self, obj, event):
#         obj.GetPlanes(self.planes)
#         self.volumeMapper0.SetClippingPlanes(self.planes)

    def cameraAnyEvent(self,obj,evt):
        self.cameraText.SetInput("Orientation X = %5.0f\nOrientation Y = %5.0f\nOrientation Z = %5.0f" % (obj.GetOrientation()[0],obj.GetOrientation()[1],obj.GetOrientation()[2]))            
           
    def displayCameraOrientation(self):
        if self.button_cameratext.isChecked():
            self._ren.AddActor(self.cameraText)
        else:
            self._ren.RemoveActor(self.cameraText)
            
        self._renWin.Render()
        
    def VTKtoNP4x4(self, vtkMat):
        r = np.zeros((4,4))
        for x,y in [(x,y) for x in range(4) for y in range(4)]:
            r[x][y] = vtkMat.GetElement(x,y)
        return r
    
    def printMatrix4(self, matrix):
        for col in range(4):
            for row in range(4):
                print matrix.GetElement(col, row),
            print ' '
        print ' '

    def updateTFunc(self):
        self.create_color_opacity_table(tfuncdir + str(self.transferFunctionControlItems.combobox_transfunction.itemText(self.transferFunctionControlItems.combobox_transfunction.currentIndex())))
        self._renWin.Render()

    def cropVolume(self):
        self.volumeMapper0.SetCroppingRegionPlanes(self.scale_xmin.value()*self.spacing[0], self.scale_xmax.value()*self.spacing[0],\
                                                  self.scale_ymin.value()*self.spacing[1], self.scale_ymax.value()*self.spacing[1],\
                                                  self.scale_zmin.value()*self.spacing[2], self.scale_zmax.value()*self.spacing[2])
        self.volumeMapper0.CroppingOn()
        self._renWin.Render()
        
    def updateVolume1Position(self):
        self.volume1.SetPosition(self.volume0.GetPosition()[0], self.volume0.GetPosition()[1], self.volume0.GetPosition()[2])
        self.volume1.SetOrientation(self.volume0.GetOrientation()[0], self.volume0.GetOrientation()[1], self.volume0.GetOrientation()[2])
        self.volume1.GetUserTransform().DeepCopy(self.volume0.GetUserTransform())
        
        
    def test0(self):
        pass
        
    def test1(self):
        pass  
        
    def report(self):
        if True:
            print 'Camera:'
            print 'pos=%s' %str(self.camera.GetPosition())
            print 'foc=%s' %str(self.camera.GetFocalPoint())
        if True:
            print'Volume0'
#             print 'oriXYZ=%s' %str(self.volume0.GetOrientation())
#             print str(self.volume0.GetMatrix())
#             print str(self.volume0.GetUserMatrix())
        if True:
            print'Volume1'
#             print 'oriXYZ=%s' %str(self.volume1.GetOrientation())
#             print str(self.volume1.GetMatrix())
#             print str(self.volume1.GetUserMatrix())
        if False:
            print'TestActor'
            print 'pos=%s' %str(self.testActor.GetPosition())
            print str(self.testActor.GetBounds())
            print 'oriXYZ=%s' %str(self.testActor.GetOrientation())
            print str(self.testActor.GetMatrix())
            print str(self.testActor.GetUserMatrix())
        if False:
            print 'SPW'
            print 'origin=%s' %str(self.splitPlaneWidget0.GetOrigin())
            print self.spwOriginR0
            print self.spwNormalR0
            print 'normal=%s' %str(self.splitPlaneWidget0.GetNormal())
            
    def thing1(self):
        print 'THING1'
        
    def thing2(self):
        print 'THING2'
        
    def read_Philips_echo_volume(self,filename):
        header = dicom.read_file(filename)
        spacing_x, spacing_y, spacing_z = header.PhysicalDeltaX, header.PhysicalDeltaY, header[0x3001, 0x1003].value
        dim_x, dim_y, dim_z, number_of_frames = header.Columns, header.Rows, header[0x3001, 0x1001].value, header.NumberOfFrames
        raw = np.fromstring(header.PixelData, dtype=np.uint8)
        return np.reshape(raw, (dim_x, dim_y, dim_z, number_of_frames), order="F"), (10*spacing_x, 10*spacing_y, 10*spacing_z), header.SOPInstanceUID
    
    def loadEcho(self):        
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.fname = QFileDialog.getOpenFileName(self,"Select DICOM File",initfdir,"All Files (*);;Text Files (*.txt)", "", options)        
        
        if self.fname:
            
            self.volT = []
            self.cb = []            
            
            self.volT, self.spacing, self.sopuid = self.read_Philips_echo_volume(str(self.fname))
            self.dim = self.volT.shape
            
            vol = self.volT[:, :, :, 0]
            
            self.reader = vtk.vtkImageImport()
            data_string = vol.flatten("A")
            self.reader.CopyImportVoidPointer(data_string, len(data_string))
            self.reader.SetDataScalarTypeToUnsignedChar()
            self.reader.SetNumberOfScalarComponents(1)
            self.reader.SetDataExtent(0, self.dim[0] - 1, 0, self.dim[1] - 1, 0, self.dim[2] - 1)
            self.reader.SetWholeExtent(0, self.dim[0] - 1, 0, self.dim[1] - 1, 0, self.dim[2] - 1)
            self.reader.SetDataSpacing(self.spacing[0], self.spacing[1], self.spacing[2]) 
 
#             self.volumeMapper0 = vtk.vtkFixedPointVolumeRayCastMapper()
            self.volumeMapper0 = vtk.vtkOpenGLVolumeTextureMapper3D()
            self.volumeMapper1 = vtk.vtkOpenGLVolumeTextureMapper3D()
    
            self.loadData()
            
            self._ren.AutomaticLightCreationOff()
            
            self.isplay = False
            self.isrotate = False
            
            self.slider_imageNumber.setEnabled(True)
            self.button_iterate.setEnabled(True)             
             
            self.slider_imageNumber.setMaximum(self.volT.shape[3]-1)
            self.slider_imageNumber.setValue(0)
            self.slider_imageNumber_valuechanged()
            self.cb = vtkTimerCallBack(self.volT.shape[3], self.isplay, self.isrotate, self.slider_imageNumber)
            
            self.cb.renderer = self._ren
    
    def loadDir(self):
        options = QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly | QFileDialog.DontUseNativeDialog
        self.dirname = str(QFileDialog.getExistingDirectory(self,"Select DICOM Directory", initddir, options))
        
        if self.dirname:
            subdir = [name for name in os.listdir(self.dirname) if os.path.isdir(os.path.join(self.dirname, name))]            
            while subdir and self.dirname:
                self.dirname = str(QFileDialog.getExistingDirectory(self, "Select DICOM Directory", self.dirname, options))
                if (self.dirname):
                    subdir = [name for name in os.listdir(self.dirname) if os.path.isdir(os.path.join(self.dirname, name))]        
                
        if self.dirname:
            self._iren.RemoveObservers('TimerEvent')
            
            self.volT = []
            self.cb = []            
            
            self.reader = vtk.vtkDICOMImageReader()
            self.reader.SetDirectoryName(self.dirname)
            self.reader.Update()
            
            self.spacing, self.sopuid = self.reader.GetOutput().GetSpacing(), self.reader.GetStudyUID()
            self.dim = self.reader.GetOutput().GetDimensions()           
            
#             self.volumeMapper0 = vtk.vtkFixedPointVolumeRayCastMapper()
#             self.volumeMapper0 = vtk.vtkOpenGLGPUVolumeRayCastMapper()            
            self.volumeMapper0 = vtk.vtkVolumeTextureMapper3D()
            self.volumeMapper1 = vtk.vtkVolumeTextureMapper3D()
                                    
            self.slider_imageNumber.setEnabled(False)
            self.slider_imageNumber.setValue(0)
            self.button_iterate.setEnabled(False)  
            
            self.loadData()
               
    def loadData(self):
        self.imageGaussianSmooth.SetInputConnection(self.reader.GetOutputPort()) 
        self.imageGaussianSmooth.Update()
        self.smoothVolume()
        
        self.volumeMapper0.SetInputConnection(self.imageGaussianSmooth.GetOutputPort())
#         self.volumeMapper0.SetPreferredMethodToNVidia()
        self.volumeMapper0.SetSampleDistance(0.5)
        self.volumeMapper0.CroppingOff()
        self.volume0.SetMapper(self.volumeMapper0)
        self.volume0.SetProperty(self.volumeProperty0)
        self.volume0.SetPosition((-self.volume0.GetCenter()[0] / 2, -self.volume0.GetCenter()[1] / 2, -self.volume0.GetCenter()[2] / 2))  
        self._ren.AddVolume(self.volume0)     
        
        self.volumeMapper1.SetInputConnection(self.imageGaussianSmooth.GetOutputPort())
#         self.volumeMapper1.SetPreferredMethodToNVidia()
        self.volumeMapper1.SetSampleDistance(0.5)
        self.volumeMapper1.CroppingOff()
        self.volume1.SetMapper(self.volumeMapper1)
        self.volume1.SetProperty(self.volumeProperty1)
        self.volume1.SetPosition(0, 0, 0)
        self._ren.AddVolume(self.volume1)
        self.volume1.VisibilityOff()
        self.volume1Enabled = False               
          

        for i in range(6):
            self.planeWidget[i].SetProp3D(self.volume0)
            self.planeWidget[i].PlaceWidget()
            self.planeWidget[i].SetInteractor(self._iren)
            self.removeAllMouseEvents(self.planeWidget[i])
        self.resetAllPlaneWidgets()
         
#         self.boxWidget.SetProp3D(self.volume0)
#         self.resetBoxWidget()  
         
        self.splitPlaneWidget0.SetProp3D(self.volume0)
        self.splitPlaneWidget0.PlaceWidget()
        self.splitPlaneWidget0.SetOrigin([0, self.volume0.GetCenter()[1], self.volume0.GetCenter()[2]]) #this needs to be later, as volume0 is not finalized yet
        self.splitPlaneWidget0.SetNormal(1,0,0)
        self.spwPlane0Callback()
        self.splitPlaneWidget0.Modified()
        
        self.splitPlaneWidget1.SetProp3D(self.volume1)
        self.splitPlaneWidget1.PlaceWidget()
        self.splitPlaneWidget1.SetOrigin([0, self.volume1.GetCenter()[1], self.volume1.GetCenter()[2]]) #this needs to be later, as volume0 is not finalized yet
        self.splitPlaneWidget1.SetNormal(1,0,0)
#         self.spwPlane0Callback()
        self.splitPlaneWidget1.Modified()
       
        self.create_color_opacity_table(tfuncdir + str(self.transferFunctionControlItems.combobox_transfunction.itemText(self.transferFunctionControlItems.combobox_transfunction.currentIndex())))
        
        for scale, idim, val in zip((self.scale_xmin,self.scale_ymin,self.scale_zmin,self.scale_xmax,self.scale_ymax,self.scale_zmax),(0,1,2,0,1,2),(0,0,0,self.dim[0],self.dim[1],self.dim[2])):
            scale.setMaximum(self.dim[idim])
            scale.setMinimum(0)
            scale.setValue(val)
            scale.setEnabled(True)
    
        for comp in (self.button_resetcrop, self.button_box):
            comp.setEnabled(True)
        for i in range(6):
            self.planeWidgetControlItems.button_pwidgets[i].setEnabled(True)
            self.planeWidgetControlItems.button_pwidgetreset[i].setEnabled(True)
        self.planeWidgetControlItems.button_pwdigetresetall.setEnabled(True)
        self.button_cursor.setEnabled(True)            
        self.combobox_loadsettings.clear() 
                   
        setting_files = glob.glob1(settings_dir+os.sep+self.sopuid, "*.xml")
        if setting_files:
            self.combobox_loadsettings.addItems(setting_files)  
            self.combobox_loadsettings.setCurrentIndex(self.combobox_loadsettings.findText("last-settings.xml"))                        

        self.lightingControlItems.button_shade.setEnabled(True)
        self.lightingControlItems.button_interpolation.setEnabled(True)
        self.lightingControlItems.button_gradientopacity.setEnabled(True)
        self.labelControlItems.button_label.setChecked(False)
        
        for i in range(self.labelControlItems.combobox_labels.count()):
            self.labelLineRep[i].SetPoint1WorldPosition(self.reader.GetOutput().GetCenter())
            self.labelLineRep[i].SetPoint2WorldPosition(self.reader.GetOutput().GetOrigin())
        
        self.initStylus()
#         self.initGrid()

        self.distanceWidget.GetDistanceRepresentation().SetPoint1WorldPosition(np.array([0,0,-100]))
        self.distanceWidget.GetDistanceRepresentation().SetPoint2WorldPosition(np.array([0,0,-200])) 
        
        self._ren.ResetCamera()     
        self._ren.ResetCameraClippingRange()
        self.camera.SetPosition(0, 0, self.camera.GetPosition()[2])
        self.camera.SetFocalPoint(0, 0, 0)

        headtrack = vtkTimerHeadTrack(self.camera, self.headtracktext, self.stylustext, self.cursor, self.volume0, self.volume1, self)
        headtrack.renderer = self._ren
        self._iren.AddObserver('TimerEvent', headtrack.execute)
        self._iren.CreateRepeatingTimer(20)
        self.volume0.AddObserver(vtk.vtkCommand.AnyEvent, self.volumeCallback0)  
                     
        self._renWin.Render()
                       
    def create_color_opacity_table(self,fname):    
        tree = ET.parse(fname)
    
        colorFunc = vtk.vtkColorTransferFunction()
        scalaropacityFunc = vtk.vtkPiecewiseFunction()
        gradientopacityFunc = vtk.vtkPiecewiseFunction()    
        
        rgbbranch = tree.iter('RGBTransferFunction')
        rgbsubbranch = rgbbranch.next().iter('ColorTransferFunction')
        for elem in rgbsubbranch.next():
            rgbval = elem.get('Value').split(" ")
            colorFunc.AddRGBPoint(float(elem.get('X')), float(rgbval[0]), float(rgbval[1]), float(rgbval[2]), float(elem.get('MidPoint')), float(elem.get('Sharpness')))    
    
        scalaropacitybranch = tree.iter('ScalarOpacity')
        scalaropacitysubbranch = scalaropacitybranch.next().iter('PiecewiseFunction')
        for elem in scalaropacitysubbranch.next():
            scalaropacityFunc.AddPoint(float(elem.get('X')), float(elem.get('Value')), float(elem.get('MidPoint')), float(elem.get('Sharpness')))
    
        gradientopacitybranch = tree.iter('GradientOpacity')
        gradientopacitysubbranch = gradientopacitybranch.next().iter('PiecewiseFunction')
        for elem in gradientopacitysubbranch.next():
            gradientopacityFunc.AddPoint(float(elem.get('X')), float(elem.get('Value')), float(elem.get('MidPoint')), float(elem.get('Sharpness')))
    
        volumeProp = tree.iter("VolumeProperty").next()
        component = tree.iter('Component').next()
        
        self.volumeProperty0.SetSpecularPower(float(component.get('SpecularPower')))
        self.volumeProperty0.SetDisableGradientOpacity(int(component.get('DisableGradientOpacity')))
        self.volumeProperty0.SetScalarOpacityUnitDistance(float(component.get('ScalarOpacityUnitDistance')))
        self.volumeProperty0.SetScalarOpacity(scalaropacityFunc)
        self.volumeProperty0.SetColor(colorFunc)
        self.volumeProperty0.SetGradientOpacity(gradientopacityFunc)
        
        self.volumeProperty1.SetSpecularPower(float(component.get('SpecularPower')))
        self.volumeProperty1.SetDisableGradientOpacity(int(component.get('DisableGradientOpacity')))
        self.volumeProperty1.SetScalarOpacityUnitDistance(float(component.get('ScalarOpacityUnitDistance')))
        self.volumeProperty1.SetScalarOpacity(scalaropacityFunc)
        self.volumeProperty1.SetColor(colorFunc)
        self.volumeProperty1.SetGradientOpacity(gradientopacityFunc)    
        
        self.lightingControlItems.slider_ambient.setValue(100*float(component.get('Ambient')))  
        self.lightingControlItems.slider_diffuse.setValue(100*float(component.get('Diffuse')))
        self.lightingControlItems.slider_specular.setValue(100*float(component.get('Specular')))
        self.lightingControlItems.button_shade.setChecked(int(component.get('Shade')))
        self.lightingControlItems.button_interpolation.setChecked(int(volumeProp.get('InterpolationType')))
        
        self.adjustLights()
        self.setInterpolation()
        self.setShade()
        
    def setBoxWidget(self):
        if self.boxWidget.GetEnabled():
            self.boxWidget.Off()
        else:
            self.boxWidget.On()       
        self._renWin.Render() 
        
    def setPlaneWidgets(self):
        self.volumeMapper0.RemoveAllClippingPlanes() #Removes planes from pwClippingPlanes
        self.volumeMapper0.SetClippingPlanes(self.pwPlanes)
        for i in range(6):
            if self.planeWidgetControlItems.button_pwidgets[i].isChecked():
                self.planeWidget[i].On()
                self.removeAllMouseEvents(self.planeWidget[i])                
                self.pwClippingPlanes.AddItem(self.pwPlane[i])
            else:
                self.planeWidget[i].Off()
                
    def setSplitPlaneWidget(self):
        if self.splitPlaneWidget0.GetEnabled():
            self.splitPlaneWidget0.Off()
            self.button_split.setEnabled(False)
        else:
            self.splitPlaneWidget0.On()
            self.button_split.setEnabled(True)
        self._renWin.Render()
        
    def setSplitVolume(self):   
        if self.volume1Enabled == True:
            self.volume1Enabled = False
            self.volume1.VisibilityOff()
            self.splitPlaneWidget1.Off()
            self.button_cursor.setEnabled(True)
            self.button_split.setText('Split')
            self.volumeMapper0.RemoveAllClippingPlanes()
            self.volumeMapper1.RemoveAllClippingPlanes()
            self.splitPlaneWidget0.GetPlaneProperty().SetOpacity(0.5)
            self.splitPlaneWidget1.GetPlaneProperty().SetOpacity(0.5)
            self.setSplitPlaneWidget()   
        else:
            self.volume1Enabled = True
            self.updateVolume1Position()
            self.volume1.VisibilityOn()
            self.splitPlaneWidget1.PlaceWidget()
            self.splitPlaneWidget1.SetOrigin(self.splitPlaneWidget0.GetOrigin()[0], self.splitPlaneWidget0.GetOrigin()[1], self.splitPlaneWidget0.GetOrigin()[2])
            self.splitPlaneWidget1.SetNormal(self.splitPlaneWidget0.GetNormal()[0], self.splitPlaneWidget0.GetNormal()[1], self.splitPlaneWidget0.GetNormal()[2])
            self.spwPlane1Callback()
            self.splitPlaneWidget1.On()
            self.button_cursor.setEnabled(False)
            self.button_split.setText('Merge')
            self.volume1.AddObserver(vtk.vtkCommand.AnyEvent, self.volumeCallback1)
            
            self.applySplitClip0()
            self.applySplitClip1()
            self.splitPlaneWidget0.GetPlaneProperty().SetOpacity(0)
            self.splitPlaneWidget1.GetPlaneProperty().SetOpacity(0)
            
    def setStereo(self):
        if self._renWin.GetStereoRender():
            self._renWin.StereoRenderOff()
        else:
            self._renWin.StereoRenderOn()
            
        self._renWin.Render()
        
    def setShade(self):
        if self.lightingControlItems.button_shade.isChecked():
            self.volumeProperty0.ShadeOn()
            self.lightingControlItems.slider_ambient.setValue(100*self.volumeProperty0.GetAmbient())
            self.lightingControlItems.slider_diffuse.setValue(100*self.volumeProperty0.GetDiffuse())
            self.lightingControlItems.slider_specular.setValue(100*self.volumeProperty0.GetSpecular())
            
            for comp in (self.lightingControlItems.slider_ambient, self.lightingControlItems.slider_diffuse, self.lightingControlItems.slider_specular, self.lightingControlItems.slider_keylightintensity):
                comp.setEnabled(True)
            
        else:
            self.volumeProperty0.ShadeOff()
            
        self._renWin.Render()
        
    def setInterpolation(self):
        if self.lightingControlItems.button_interpolation.isChecked():
            self.volumeProperty0.SetInterpolationTypeToLinear()
        else:
            self.volumeProperty0.SetInterpolationTypeToNearest()
                        
        self._renWin.Render()
        
    def setDisableGradientOpacity(self):
        if self.lightingControlItems.button_gradientopacity.isChecked():
            self.volumeProperty0.DisableGradientOpacityOn()
        else:
            self.volumeProperty0.DisableGradientOpacityOff()
            
        self._renWin.Render()

    def setMeasurement(self):
        if self.distanceWidget.GetEnabled(): 
            self.distanceWidget.EnabledOff()
            self._ren.RemoveActor(self.distanceText) 
        else:
            self.distanceWidget.EnabledOn()
            self.removeAllMouseEvents(self.distanceWidget)
            self._ren.AddActor(self.distanceText) 
        
        self._renWin.Render()

    def setAngleMeasurement(self):
        if self.angleWidget.GetEnabled(): 
            self.angleWidget.EnabledOff()
            self._ren.RemoveActor(self.angleText) 
        else:
            self.angleWidget.EnabledOn()
            self._ren.AddActor(self.angleText) 
        
        self._renWin.Render()

    def saveSettings(self):
        
        if self.sopuid:
            self.pwClippingPlanes.GetMTime()
                
            root = ET.Element("root")    
            sliders = ET.SubElement(root, "sliders")
                
            for sldr, txt in zip((self.scale_xmin,self.scale_ymin,self.scale_zmin,self.scale_xmax,self.scale_ymax,self.scale_zmax),('x_min_slider', 'y_min_slider', 'z_min_slider', 'x_max_slider', 'y_max_slider', 'z_max_slider')):
                sliders.set(txt,str(sldr.value()))   
                
            bwtransform = vtk.vtkTransform()
            self.boxWidget.GetTransform(bwtransform)
            
            bwtransformsettings = ET.SubElement(root, "boxwidgettransform")
            buf = "" 
            for i in range(4):
                for j in range(4):
                    buf += str(bwtransform.GetMatrix().GetElement(i,j)) + ","
        
            bwtransformsettings.set("elements", buf[0:-1])
            bwtransformsettings.set("mtime", str(self.boxWidget.GetMTime()))
                  
            camera = ET.SubElement(root, "camera")
            camera.set("Position", str(self._ren.GetActiveCamera().GetPosition()))
            camera.set("FocalPoint", str(self._ren.GetActiveCamera().GetFocalPoint()))
            camera.set("ViewUp", str(self._ren.GetActiveCamera().GetViewUp()))
            
            planewidgetsettings = ET.SubElement(root, "planewidgetsettings")
            planewidgetsettings.set("mtime", str(self.pwClippingPlanes.GetMTime()))
            for i in range(6):
                planewidgetsettings.set("origin"+str(i), str(self.pwClippingPlanes.GetItem(i).GetOrigin()))
                planewidgetsettings.set("normal"+str(i), str(self.pwClippingPlanes.GetItem(i).GetNormal()))
            
            
            tfuncsettings = ET.SubElement(root, "tfunc")
            tfuncsettings.set("filename", str(self.transferFunctionControlItems.combobox_transfunction.itemText(self.transferFunctionControlItems.combobox_transfunction.currentIndex())))
            
            
            volPropSettings = ET.SubElement(root,'volumepropertysettings')
            rgbfuncsettings = ET.SubElement(volPropSettings,'rgbfuncsettings')
            rgbfuncsettings.set('NumberOfPoints',str(self.volumeProperty0.GetRGBTransferFunction(0).GetSize()))
            val = np.empty((6))
            for i in range(self.volumeProperty0.GetRGBTransferFunction(0).GetSize()):
                self.volumeProperty0.GetRGBTransferFunction(0).GetNodeValue(i, val)
                rgbfuncsettings.set('pt'+str(i),"%d, %f, %f, %f, %f, %f" % (val[0],val[1],val[2],val[3],val[4],val[5]))
                
            scalarfuncsettings = ET.SubElement(volPropSettings,'scalarfuncsettings')        
            scalarfuncsettings.set('NumberOfPoints',str(self.volumeProperty0.GetScalarOpacity(0).GetSize()))
            val = np.empty((4))
            for i in range(self.volumeProperty0.GetScalarOpacity(0).GetSize()):    
                self.volumeProperty0.GetScalarOpacity(0).GetNodeValue(i, val)
                scalarfuncsettings.set('pt'+str(i),"%d, %f, %f, %f" % (val[0],val[1],val[2],val[3]))
            
            gradientfuncsettings = ET.SubElement(volPropSettings,'gradientfuncsettings')        
            gradientfuncsettings.set('NumberOfPoints',str(self.volumeProperty0.GetGradientOpacity(0).GetSize()))
            for i in range(self.volumeProperty0.GetGradientOpacity(0).GetSize()):    
                self.volumeProperty0.GetGradientOpacity(0).GetNodeValue(i, val)
                gradientfuncsettings.set('pt'+str(i),"%d, %f, %f, %f" % (val[0],val[1],val[2],val[3]))    
            
                   
            tree = ET.ElementTree(root)
            
            settings_subdir = self.sopuid            
            
            if not os.path.isdir(settings_dir):
                os.mkdir(settings_dir)
                
            if not os.path.isdir(settings_dir+os.sep+settings_subdir):
                os.mkdir(settings_dir+os.sep+settings_subdir)                
                
            if os.path.isfile(settings_dir + os.sep + settings_subdir+os.sep+"last-settings.xml"):
                os.rename(settings_dir + os.sep + settings_subdir+os.sep+"last-settings.xml", settings_dir + os.sep + settings_subdir+os.sep+datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")+".xml")
               
            
            options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
            filepath = QFileDialog.getSaveFileName(self,"Save Current Settings",settings_dir+ os.sep + settings_subdir+ os.sep + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"),"XML Files (*.xml)", "XML Files (*.xml)",options)
                
            if filepath:
                _, ext = os.path.splitext(str(filepath))
                if ext.upper() != '.XML':
                    filepath = '%s.xml' % filepath

                tree.write(filepath)  

                filedir, fname = os.path.split(str(filepath))
            
                self.combobox_loadsettings.clear()            
                setting_files = glob.glob1(settings_dir+os.sep+self.sopuid, "*.xml")
                if setting_files:
                    self.combobox_loadsettings.addItems(setting_files)   
                    self.combobox_loadsettings.setCurrentIndex(self.combobox_loadsettings.findText(fname+".xml"))
        
    def loadSettings(self):
        if self.sopuid:
            fname = self.sopuid
            tree = None
            
            setting_fname = str(self.combobox_loadsettings.itemText(self.combobox_loadsettings.currentIndex()))
            
            if os.path.isfile(settings_dir + os.sep + fname+os.sep+setting_fname):
                tree = ET.parse(settings_dir + os.sep + fname+os.sep+setting_fname)  
            else:
                options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
                fname = QFileDialog.getOpenFileName(self,"Select Settings File",settings_dir,"All Files (*);;XML Files (*.xml)", "", options)
                if fname:
                    tree = ET.parse(fname)
            
            if tree:
                sliders = tree.iter('sliders').next()
             
                for sldr, txt in zip((self.scale_xmin,self.scale_ymin,self.scale_zmin,self.scale_xmax,self.scale_ymax,self.scale_zmax),('x_min_slider', 'y_min_slider', 'z_min_slider', 'x_max_slider', 'y_max_slider', 'z_max_slider')): 
                    sldr.setValue(int(sliders.get(txt)))
                    
                    
                bwtransformsettings = tree.iter('boxwidgettransform').next()                    
                tmatrix = np.fromstring(bwtransformsettings.get("elements"), sep=",")
                
                bwtransform = vtk.vtkTransform()
                bwmatrix = vtk.vtkMatrix4x4()
                for i in range(4):
                    for j in range(4):
                        bwmatrix.SetElement(i,j,tmatrix[4*i+j])
                     
                bwtransform.SetMatrix(bwmatrix)
                self.boxWidget.SetTransform(bwtransform)
                self.bwClipVolumeRender(self.boxWidget, None)
                
                
                if tree.findall('planewidgetsettings'):
                    planewidgetsettings = tree.iter('planewidgetsettings').next()
                    self.pwClippingPlanes.RemoveAllItems()
                    for i in range(6):
                        self.pwPlane[i].SetOrigin(np.fromstring(planewidgetsettings.get("origin"+str(i))[1:-1], sep=","))
                        self.pwPlane[i].SetNormal(np.fromstring(planewidgetsettings.get("normal"+str(i))[1:-1], sep=","))
                        self.pwClippingPlanes.AddItem(self.pwPlane[i])
                        self.planeWidget[i].SetOrigin(self.pwPlane[i].GetOrigin()[0],self.pwPlane[i].GetOrigin()[1],self.pwPlane[i].GetOrigin()[2])
                        self.planeWidget[i].SetNormal(self.pwPlane[i].GetNormal())
                        self.planeWidget[i].UpdatePlacement()                    
                    
                    if bwtransformsettings.get('mtime') and planewidgetsettings.get('mtime'):
                        bwtime = int(bwtransformsettings.get('mtime'))
                        pwtime = int(planewidgetsettings.get('mtime'))                                        
                        if pwtime > bwtime:
                            self.pwCallback(self, None)
                    

                camera = tree.iter('camera').next()
                self._ren.GetActiveCamera().SetPosition(np.fromstring(camera.get("Position")[1:-1], sep=","))
                self._ren.GetActiveCamera().SetFocalPoint(np.fromstring(camera.get("FocalPoint")[1:-1], sep=","))
                self._ren.GetActiveCamera().SetViewUp(np.fromstring(camera.get("ViewUp")[1:-1], sep=",")) 
                
                tfuncsetting = tree.iter("tfunc").next()
                self.transferFunctionControlItems.combobox_transfunction.setCurrentIndex(self.transferFunctionControlItems.combobox_transfunction.findText(tfuncsetting.get("filename")))
                self.create_color_opacity_table(tfuncdir + str(self.transferFunctionControlItems.combobox_transfunction.itemText(self.transferFunctionControlItems.combobox_transfunction.currentIndex())))
                
                rgbfunc = self.volumeProperty0.GetRGBTransferFunction(0)
                rgbfunc.RemoveAllPoints()
                
                volumepropertysettings = tree.iter('volumepropertysettings').next()
                rgbfunctsettings = volumepropertysettings.iter('rgbfuncsettings').next()
                for i in range(int(rgbfunctsettings.get('NumberOfPoints'))):
                    val = np.fromstring(rgbfunctsettings.get('pt'+str(i)),sep=",")
                    rgbfunc.AddRGBPoint(val[0],val[1],val[2],val[3],val[4],val[5])
                    
                scalarfunc = self.volumeProperty0.GetScalarOpacity(0)
                scalarfunc.RemoveAllPoints()            
        
                scalarfuncsettings = volumepropertysettings.iter('scalarfuncsettings').next()
                for i in range(int(scalarfuncsettings.get('NumberOfPoints'))):
                    val = np.fromstring(scalarfuncsettings.get('pt'+str(i)),sep=",")
                    scalarfunc.AddPoint(val[0],val[1],val[2],val[3])
                    
                gradientfunc = self.volumeProperty0.GetGradientOpacity(0)
                gradientfunc.RemoveAllPoints()            
        
                gradientfuncsettings = volumepropertysettings.iter('gradientfuncsettings').next()
                for i in range(int(gradientfuncsettings.get('NumberOfPoints'))):
                    val = np.fromstring(gradientfuncsettings.get('pt'+str(i)),sep=",")
                    gradientfunc.AddPoint(val[0],val[1],val[2],val[3]) 
                        
            self.setShade()
            self._renWin.Render()

    def playCardiacCycle(self):
        if self.cb:
            if not self.isplay:
                self.isplay = True
                self.tag_observer1 = self._iren.AddObserver('TimerEvent', self.cb.execute)
                self._iren.CreateRepeatingTimer(10)
                self.cb.setplay(True)
            else:
                self.isplay = False
                self.cb.setplay(False)
                if not self.isrotate:
                    self._iren.RemoveObserver(self.tag_observer1)
    
    def rotateCamera(self):
        if self.cb:
            if not self.isrotate:
                self.isrotate = True
                self._iren.AddObserver('TimerEvent', self.cb.execute)
                self._iren.CreateRepeatingTimer(20)
                self.cb.setrotate(True)
            else:
                self.isrotate = False
                self.cb.setrotate(False)       
                if not self.isplay:
                    self._iren.RemoveObservers('TimerEvent')

    def editTransferFunction(self):
        if self.reader:
            self.transferFunctionEditor = TransferFunctionEditor(self.volumeProperty0, self.reader, self._renWin)
            layout = pysideQGridLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            layout.addWidget(self.transferFunctionEditor.getTransferFunctionWidget())
             
            self.transferFunctionEditor.setLayout(layout)
            self.transferFunctionEditor.show()
                        
    def setStereoDepth(self, evt):
        self._ren.GetActiveCamera().SetEyeAngle(0.1*self.scale_stereodepth.value())
        self._renWin.Render()
        
    def setAzimuth(self, evt):
        val = self.scale_azimuth.value()
        self._ren.GetActiveCamera().Azimuth(self.varscaleazimuth-val)
        self.varscaleazimuth = val
        self._renWin.Render()
        
    def setElevation(self,evt):
        val = self.scale_elevation.value()
        self._ren.GetActiveCamera().Elevation(self.varscaleelevation-val)
        self.camerapos = self._ren.GetActiveCamera().OrthogonalizeViewUp() 
        self.varscaleelevation = val
        self._renWin.Render()

    def setRoll(self,evt):
        self._ren.GetActiveCamera().SetRoll(-1.0*self.scale_roll.value())
        self._renWin.Render()

    def zoomIn(self):
        self._ren.GetActiveCamera().Zoom(1.1)
        self._ren.ResetCameraClippingRange()
        self._renWin.Render()

    def zoomOut(self):
        self._ren.GetActiveCamera().Zoom(1.0/1.1)
        self._ren.ResetCameraClippingRange()
        self._renWin.Render()

    def resetCamera(self):
        self._ren.ResetCamera()
        self._ren.ResetCameraClippingRange()
        self._renWin.Render()
        
    def editOpacity(self):
        if self.reader:  
            self.opacityEditor = OpacityEditor(self.volumeProperty0, self.reader, self._renWin)              
            self.opacityEditor.show()          

    def editGradientOpacity(self):
        if self.reader:                 
            self.opacityGradientEditor = GradientOpacityEditor(self.volumeProperty0, self.reader, self._renWin)              
            self.opacityGradientEditor.show()   
    
    def editColor(self):
        if self.reader:     
            self.colorEditor = ColorEditor(self.volumeProperty0, self.reader, self._renWin)
            self.colorEditor.show()
        
    def slider_imageNumber_valuechanged(self):
        if self.cb:
            self.label_imageNumber.setText(str(self.slider_imageNumber.value() + 1) + "/" + str(self.slider_imageNumber.maximum() + 1))    
            vol = self.volT[:, :, :, self.slider_imageNumber.value()]
            data_string = vol.flatten("A")
            self.reader.CopyImportVoidPointer(data_string, len(data_string)) 
            self.imageGaussianSmooth.Update()
            self._renWin.Render()    
            
    def smoothVolume(self):
        self.imageGaussianSmooth.SetStandardDeviations(0.1*self.slider_xsmooth.value(),0.1*self.slider_ysmooth.value(),0.1*self.slider_zsmooth.value())
        self.imageGaussianSmooth.Update()
        for label, slider in zip((self.label_xsmooth,self.label_ysmooth,self.label_zsmooth),(self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth)):
            label.setText(str(slider.value()*0.1))   
            
        self._renWin.Render()
        
    def setNoSmooth(self):
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.setValue(0.0)
        
        self.smoothVolume()

    def setLowSmooth(self):
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.setValue(1)
        
        self.smoothVolume()
        
    def setMidSmooth(self):
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.setValue(10)
        
        self.smoothVolume()
        
    def setHighSmooth(self):
        for slider in (self.slider_xsmooth,self.slider_ysmooth,self.slider_zsmooth):
            slider.setValue(50)
        
        self.smoothVolume()                

    def adjustLights(self):
        
        self.lightingControlItems.label_ambient.setText('Ambient: %.2f' % (0.01*self.lightingControlItems.slider_ambient.value()))
        self.lightingControlItems.label_diffuse.setText('Diffuse: %.2f' % (0.01*self.lightingControlItems.slider_diffuse.value()))
        self.lightingControlItems.label_specular.setText('Specular: %.2f' % (0.01*self.lightingControlItems.slider_specular.value()))
                
        self.volumeProperty0.SetAmbient(0.01*self.lightingControlItems.slider_ambient.value())
        self.volumeProperty0.SetDiffuse(0.01*self.lightingControlItems.slider_diffuse.value())
        self.volumeProperty0.SetSpecular(0.01*self.lightingControlItems.slider_specular.value())     
        
        self._renWin.Render()
        
    def setKeyLightIntensity(self):
        val = 0.2*self.lightingControlItems.slider_keylightintensity.value()
        
        self.lightingControlItems.label_keylightintensity.setText("Key Light Intensity: %.1f" % (val))
        
        self.lightkit.SetKeyLightIntensity(val)
        self._renWin.Render()
                
    def saveScreen(self):            
        options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
        filepath = QFileDialog.getSaveFileName(self,"Save Current Settings","","JPG Files (*.jpg)", "JPG Files (*.jpg)",options)
        if filepath:
            writer = vtk.vtkJPEGWriter()
            writer.SetFileName(str(filepath)+".jpg")
            writer.SetQuality(100)
            w2i = vtk.vtkWindowToImageFilter()
            w2i.SetInput(self._renWin)
            w2i.Update()
            writer.SetInputConnection(w2i.GetOutputPort())
            writer.Write()

    def changeView(self):
        str_button_view = self.sender().objectName()
        cam = self._ren.GetActiveCamera()
        focalpoint = cam.GetFocalPoint()
        dist = cam.GetDistance()
        
        if str_button_view == "button_view0":
            cam.SetPosition(focalpoint[0],focalpoint[1],focalpoint[2]+dist)
            cam.SetViewUp(0,1,0)
        if str_button_view == "button_view1":
            cam.SetPosition(focalpoint[0]+dist,focalpoint[1],focalpoint[2])
            cam.SetViewUp(0,1,0)
        if str_button_view == "button_view2":
            cam.SetPosition(focalpoint[0],focalpoint[1]+dist,focalpoint[2])
            cam.SetViewUp(0,0,-1)
        if str_button_view == "button_view3":
            cam.SetPosition(focalpoint[0],focalpoint[1],focalpoint[2]-dist)
            cam.SetViewUp(0,1,0)
        if str_button_view == "button_view4":
            cam.SetPosition(focalpoint[0]-dist,focalpoint[1],focalpoint[2])
            cam.SetViewUp(0,1,0)
        if str_button_view == "button_view5":
            cam.SetPosition(focalpoint[0],focalpoint[1]-dist,focalpoint[2])
            cam.SetViewUp(0,0,1)
        if str_button_view == "button_view6":
            self.scale_roll.setValue(0)
        if str_button_view == "button_view7":
            self.scale_roll.setValue(90)
        if str_button_view == "button_view8":
            self.scale_roll.setValue(180)
        if str_button_view == "button_view9":
            self.scale_roll.setValue(-90)
        
        self._renWin.Render()
         
    def displayLabel(self):
        i = self.labelControlItems.combobox_labels.currentIndex()        
        if self.labelControlItems.button_label.isChecked():
            self.labelActor[i].SetPosition(self.labelLineRep[i].GetPoint2WorldPosition())
            self.labelLine[i].EnabledOn()
            self._ren.AddActor(self.labelActor[i])
        else:
            self.labelLine[i].EnabledOff() 
            self._ren.RemoveActor(self.labelActor[i])
            
        self._renWin.Render()
        
    def changeLabelText(self):
        index = self.labelControlItems.combobox_labels.currentIndex()
        self.labelText[index].SetText(str(self.labelControlItems.text_label.headtrackText()))
        self._renWin.Render()
         
    def changeLabelSize(self):
        index = self.labelControlItems.combobox_labels.currentIndex()        
        self.labelActor[index].SetScale(0.1*self.labelControlItems.scale_labelsize.value())
        self._renWin.Render()
        
    def changeLabelIndex(self, value):
        self.labelControlItems.button_label.setChecked(self.labelLine[value].GetEnabled())
        self.labelControlItems.text_label.setText(self.labelText[value].GetText())
        
    def saveTransferFunction(self):
        
        root = ET.Element("TransferFunctions")          
        root.set("Type", "User")
        
        volumeProperty = ET.SubElement(root,"VolumeProperty")
        volumeProperty.set("InterpolationType", str(self.volumeProperty0.GetInterpolationType()))
                
        component0 = ET.SubElement(volumeProperty, "Component")
        component0.set("Index", "0")
        component0.set("Shade", str(self.volumeProperty0.GetShade(0)))
        component0.set("Ambient", str(self.volumeProperty0.GetAmbient(0)))
        component0.set("Diffuse", str(self.volumeProperty0.GetDiffuse(0)))
        component0.set("Specular", str(self.volumeProperty0.GetSpecular(0)))
        component0.set("SpecularPower", str(self.volumeProperty0.GetSpecularPower(0)))
        component0.set("DisableGradientOpacity", str(self.volumeProperty0.GetDisableGradientOpacity(0)))
        component0.set("ComponentWeight", str(self.volumeProperty0.GetComponentWeight(0)))
        component0.set("ScalarOpacityUnitDistance", str(self.volumeProperty0.GetScalarOpacityUnitDistance(0)))
                                
        rgbtransferfuction = ET.SubElement(component0, "RGBTransferFunction")
        colorTransferFunction = ET.SubElement(rgbtransferfuction, "ColorTransferFunction")
        rgbfunction = self.volumeProperty0.GetRGBTransferFunction(0)
        colorTransferFunction.set("Size",str(rgbfunction.GetSize()))
        
        nodeval = np.empty((6))
        for i in range(rgbfunction.GetSize()):
            rgbfunction.GetNodeValue(i, nodeval)
            point = ET.SubElement(colorTransferFunction, "Point")
            point.set("X",str(int(nodeval[0])))
            point.set("Value", "%f %f %f" % (nodeval[1], nodeval[2], nodeval[3]))
            point.set("MidPoint",str(int(nodeval[4])))
            point.set("Sharpness",str(int(nodeval[5])))

        scalarOpacity = ET.SubElement(component0, "ScalarOpacity")
        piecewiseFunction = ET.SubElement(scalarOpacity, "PiecewiseFunction")
        scalarfunction = self.volumeProperty0.GetScalarOpacity(0)
        scalarOpacity.set("Size",str(scalarfunction.GetSize()))
        
        nodeval = np.empty((4))
        for i in range(scalarfunction.GetSize()):
            scalarfunction.GetNodeValue(i, nodeval)
            point = ET.SubElement(piecewiseFunction, "Point")
            point.set("X",str(int(nodeval[0])))
            point.set("Value", "%f" % nodeval[1])
            point.set("MidPoint",str(int(nodeval[2])))
            point.set("Sharpness",str(int(nodeval[3])))


        gradientOpacity = ET.SubElement(component0, "GradientOpacity")
        piecewiseFunction = ET.SubElement(gradientOpacity, "PiecewiseFunction")
        gradientfunction = self.volumeProperty0.GetGradientOpacity(0)
        gradientOpacity.set("Size",str(gradientfunction.GetSize()))
        
        nodeval = np.empty((4))
        for i in range(gradientfunction.GetSize()):
            gradientfunction.GetNodeValue(i, nodeval)
            point = ET.SubElement(piecewiseFunction, "Point")
            point.set("X",str(int(nodeval[0])))
            point.set("Value", "%f" % nodeval[1])
            point.set("MidPoint",str(int(nodeval[2])))
            point.set("Sharpness",str(int(nodeval[3])))
                                
        
        tree = ET.ElementTree(root)        

        options = QFileDialog.Options() | QFileDialog.DontUseNativeDialog
        filepath = QFileDialog.getSaveFileName(self,"Save Transfer Function",tfuncdir+ os.sep + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"),"VVT Files (*.vvt)", "VVT Files (*.vvt)",options)
        if filepath:
            _, ext = os.path.splitext(str(filepath))
            if ext.upper() != '.VVT':
                filepath = '%s.vvt' % filepath
       
        tree.write(filepath)
        
        
        filedir, fname = os.path.split(str(filepath))
        
        self.transferFunctionControlItems.combobox_transfunction.clear()
    
        tfunc_files = glob.glob1(tfuncdir,"*.vvt")
        if tfunc_files:
            self.transferFunctionControlItems.combobox_transfunction.addItems(tfunc_files)   
            self.transferFunctionControlItems.combobox_transfunction.setCurrentIndex(self.transferFunctionControlItems.combobox_transfunction.findText(fname+".vvt"))        
                 
def main():        
    app = QApplication([])
    File = QFile("darkorange.stylesheet")
    File.open(QFile.ReadOnly)
    StyleSheet = QLatin1String(File.readAll())    
    app.setStyleSheet(StyleSheet)

    tdviz = TDVizCustom()
    tdviz.show()
    
    if isprojector:
        tdviz.setGeometry(1920, 0, 1280, 1024)
    else:
        tdviz.showFullScreen()    

    yscreenf = 1.0*tdviz._renWin.GetSize()[1]/1080.0
    cam = tdviz._ren.GetActiveCamera()
    cam.SetScreenBottomLeft(-262.5,148.5-148.5*2.0*yscreenf,-410)
    cam.SetScreenBottomRight(262.5,148.5-148.5*2.0*yscreenf,-410)
    cam.SetScreenTopRight(262.5,148.5,-410) 

    sys.exit(app.exec_())   
        
if __name__ == "__main__": 
     main()
